$(document).ready(function () {
    var is_mobile = false;
    var timeout_link;

    if ($(window).width() < 770) {
        if ($('.tournament__sidebar .js-slider-480').length) {
            var mySwiper = new Swiper('.tournament__sidebar .js-slider-480', {
                loop: true,
                pagination: {
                    el: '.swiper-pagination'
                },
                navigation: {
                    nextEl: '#rfNext-btn',
                    prevEl: '#rfPrev-btn',
                },
                scrollbar: {
                    el: '.swiper-scrollbar'
                }
            })
        }
        if ($('.tournament__main .js-slider-480').length) {
            $('.tournament__main .tournament__list').owlCarousel({
                margin: 0,
                loop: true,
                nav: false,
                navText: [,],
                dots: true,
                autoplay: false,
                items: 1
            })
        }
    }

    if (device.mobile() || $(window).width() < 767) {
        is_mobile = true;
    }

    if ($('.js--select-styled').length) {
        $('.js--select-styled').each(function () {
            var select = $(this);
            select.styler({
                onSelectClosed: function () {
                    if (select.hasClass('js--load-rating')) {
                        var url = select.data('url');
                        var method = select.data('method');
                        var data = select.find('option:selected').val();
                        console.log(data);

                        $.ajax({
                            url: url,
                            method: method,
                            data: {data: data},
                            success: function (data) {
                                var parse_data = jQuery.parseJSON(data);
                                $('.tournament__slider-box--right').html(parse_data.html);
                            }
                        });

                    }
                }
            });
        })
    }

    var current_ava = ''
    if ($('.js--file-styled').length) {
        $('.js--file-styled').styler({
            onFormStyled: function () {
                if ($('.settings').length) {
                    $('.jq-file__name').text('Загрузить файл');
                    $('.jq-file__browse').text('')
                }
            }
        });
        $('.js--file-styled').on('change', function () {
            current_ava = $('.load-img img').attr('srcset');
            readURL(this);
        })
    }
    ;

    // показать загруженную аватарку
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('.load-img img').attr('srcset', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }

    // удалить аватарку
    $('.js--remove-userpick').on('click', function () {
        $('.load-img img').attr('srcset', current_ava);
        return false;
    })

    $('.js--toggle-menu').on('click', function () {
        $(this).toggleClass('is-active');
        $('.page__nav').toggleClass('is-visible');
        //$('body').toggleClass('fixed');

        return false;
    });

    $('.js--account-dropdown').on('click', function () {
        console.log('1')

        if ($(window).width() < 992) {
            $('.header__account').toggleClass('is-clicked');
        }

        return false;
    });

    if (is_mobile && $('.winners__list').length) {
        $('.winners__list').owlCarousel({
            margin: 0,
            loop: true,
            nav: false,
            navText: [,],
            dots: true,
            autoplay: false,
            items: 1,
            responsive: {
                0: {
                    items: 1
                },
                360: {
                    items: 2
                },
                500: {
                    items: 3
                }

            }
        })
    }


    if ($('.slider-range').length) {
        var slider = $('.slider-range');


        slider.each(function () {
            var container = $(this).closest('.tabs-inner__content')

            $(this).slider({
                range: "min",
                min: slider.data('min'),
                max: slider.data('max'),
                step: slider.data('step'),
                value: slider.data('current'),
                slide: function (event, ui) {
                    $(this).closest('.tabs-inner__content').find('.js--convert-value').val(ui.value);
                    convert_count(container);
                }
            });

            // ввод только цифр в поле количетво
            $(document).on('keydown', '.js--input-number', function (e) {
                input_number();
            });

            // ввод количества с клавиатуры
            $(document).on('input', '.js--input-number', function () {
                var input = $(this);
                var val = input.val();

                if (input.data("lastval") != val) {
                    if (input.val() == '') {
                        if ($('.convert').length) {
                            input.val($('.slider-range').data('min'))
                        }
                        else {
                            input.val(0)
                        }
                    }
                    else {
                        var nimberval;

                        val = val.replace(/\s+/g, '');
                        val = Number(val);
                        nimberval = val;
                        val = val.toString();
                        val = number_format(val);
                        if (val == "NaN") {
                            if ($('.convert').length) {
                                input.val($('.slider-range').data('min'))
                            }
                            else {
                                input.val(0)
                            }
                        } else {
                            if ($('.convert').length) {
                                if (nimberval < $('.slider-range').data('min')) {
                                    input.val(number_format($('.slider-range').data('min').toString()))
                                }
                                else {
                                    if (nimberval > $('.slider-range').data('max')) {
                                        input.val(number_format($('.slider-range').data('max').toString()))
                                    }
                                    else {
                                        input.val(val)
                                    }
                                }
                            }
                            else {
                                input.val(val)
                            }
                        }
                    }
                    ;
                    $(this).data("lastval", $(this).val());

                    if (timeout_link) {
                        clearTimeout(timeout_link)
                    }
                    timeout_link = setTimeout(function () {
                        convert_count();
                    }, 250)

                }
                ;
            });

            // клик на +
            $(document).on('click', '.js--change-convert', function () {

                var container = $(this).closest('.tabs-inner__content')

                var current_value = container.find('.js--convert-value').val();
                current_value = current_value.replace(/\s+/g, '');
                current_value = Number(current_value);

                var buttom = $(this);

                if (buttom.hasClass('js--remove') == true && current_value == 0) {
                    return false
                } else {
                    if (buttom.hasClass('js--add') == true && current_value == container.find('.slider-range').data('max')) {
                        return false;
                    }
                    else {
                        (buttom.hasClass('js--remove') == true) ? (current_value--) : (current_value++);
                        current_value = current_value.toString();
                        current_value = number_format(current_value);
                        container.find('.js--convert-value').val(current_value);
                        convert_count(container);
                    }
                }

                return false;
            });
        });

        $('.js--convert-value').on('input', function () {
            var inp = $(this);
            var container = inp.closest('.tabs-inner__content')
            if (timeout_link) {
                clearTimeout(timeout_link)
            }
            timeout_link = setTimeout(function () {

                var val = $('.js--convert-value').val();

                if (val > slider.data('max')) {
                    inp.val(slider.data('max'))
                }
                if (val < slider.data('min')) {
                    inp.val(slider.data('min'))
                }
                slider.slider("value", $('.js--convert-value').val())

                convert_count(container);
            }, 250)
        })
    };


    var input_number = function () {
        var allow_meta_keys = [86, 67, 65];
        if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 ||
            ($.inArray(event.keyCode, allow_meta_keys) > -1 && (event.ctrlKey === true || event.metaKey === true)) ||
            (event.keyCode >= 35 && event.keyCode <= 39)) {
            return;
        }
        else {
            if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                event.preventDefault();
            }
        }
    };

    // разделение суммы на тройки пробелами
    function number_format(str) {
        return str.replace(/(\s)+/g, '').replace(/(\d{1,3})(?=(?:\d{3})+$)/g, '$1 ');
    }

    function convert_count(container) {
        var coins = container.find('.js--convert-value').val();
        coins = coins.replace(/\s+/g, '');
        coins = Number(coins);
        var course = container.find('.js--convert-value').data('exchange');

        container.find('.contvert__container-give .value').text(coins)
        var total = coins * course;
        container.find('.contvert__container-get .value').text(number_format(total.toString()))
    }

    $('ul.tabs__caption').each(function () {
        $(this).find('li').each(function (i) {
            $(this).click(function () {
                $(this).addClass('active').siblings().removeClass('active')
                    .closest('.tabs').find('.tabs__content').removeClass('active').eq(i).addClass('active');
            });
        });
    });

    $('ul.tabs-inner__caption').each(function () {
        $(this).find('li').each(function (i) {
            $(this).click(function () {
                $(this).addClass('active').siblings().removeClass('active')
                    .closest('.tabs-inner').find('.tabs-inner__content').removeClass('active').eq(i).addClass('active');
            });
        });
    });

    $('body').on('click', function (event) {
        var item = $(event.target);
        if (!(item.closest('div').hasClass('header__account-dropdown'))) {
            $('.header__account').removeClass('is-clicked')
        }
    });

    $('.page__nav').on('click', function (e) {
        $('.page__nav').removeClass('is-visible')
    });


    $('.js--unfreeze').on('click', function (e) {
        $(this).closest('.game-field').removeClass('freeze-game');
        $(this).closest('.game-field__modal').hide('fast');
    });


    // догрузить контент
    $(document).on('click', '.js--load-items', function () {
        var btn = $(this);
        var url = btn.data('url');
        var method = btn.data('method');
        var length = $('.load-container > li').length;

        $.ajax({
            url: url,
            method: method,
            data: {length: length},
            success: function (data) {
                var parse_data = jQuery.parseJSON(data);
                $('.load-container').append(parse_data.html);
                btn.next('span').text(parse_data.current_counter)

                if (!parse_data.has_content) {
                    btn.remove();
                }
            }
        });

        return false;
    });

    // добавить в избранное/удалить из избранного
    $(document).on('click', '.js--favourite', function () {
        var btn = $(this);
        var url = btn.data('url');
        var method = btn.data('method');
        var id = btn.data('id');

        $.ajax({
            url: url,
            method: method,
            data: {id: id},
            success: function (data) {
                btn.toggleClass('is-active');
            }
        });

        return false;
    });

    // установить фильтр
    $(document).on('click', '.js--set-filter', function () {
        var form = $(this).closest('form');
        var url = form.attr('action');
        var method = form.attr('method');
        var data = form.serialize();

        $.ajax({
            url: url,
            method: method,
            data: {data: data},
            success: function (data) {
                var parse_data = jQuery.parseJSON(data);
                $('.sorting-load-container').html(parse_data.html);
            }
        });

        return false;
    });

    // отправка формы
    function validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    $('.inp').focus(function () {
        $(this).removeClass('error')
    });

    $(document).on('click', '.js--form-submit', function () {
        var form = $(this).parents('.main-form');
        var errors = false;

        $(form).find('.required').each(function () {
            var val = $(this).prop('value');
            if (val == '') {
                $(this).addClass('error');
                errors = true;
            }
            else {
                if ($(this).hasClass('inp-mail')) {
                    if (validateEmail(val) == false) {
                        $(this).addClass('error');
                        errors = true;
                    }
                }
                if (form.find('.js--new-password').length) {
                    if (form.find('.js--new-password').val() != form.find('.js--new-password2').val()) {
                        form.find('.js--new-password').addClass('error');
                        form.find('.js--new-password2').addClass('error');
                    }
                }
            }
        });

        if (errors == false) {
            if (form.hasClass('form-refresh-form')) {
                form.trigger('submit')
            } else {
                var button_value = $(form).find('.js--form-submit').text();
                $(form).find('.js--form-submit').text('Отправляем...');

                var method = form.attr('method');
                var action = form.attr('action');
                var data = form.serialize();
                $.ajax({
                    type: method,
                    url: action,
                    data: data,
                    success: function (data) {
                        form.find('.inp').each(function () {
                            $(this).prop('value', '')
                        });
                        $(form).find('.js--form-submit').text(button_value);
                        window.location.href = "thanks.html";
                    },
                    error: function (data) {
                        $(form).find('.js--form-submit').text('Ошибка');
                        setTimeout(function () {
                            $(form).find('.js--form-submit').text(button_value);
                        }, 2000);
                    }
                });
            }
        }

        return false;
    });

    $(document).on('focus', '.inp', function () {
        $(this).removeClass('error')
    });

    init_accordeon();

    // аккордеон в футере
    function init_accordeon() {
        if (device.mobile() || $(window).width() < 767) {
            $('.footer__nav').hide();

            $('.footer__title').click(function () {
                if ($(this).next().is(':hidden')) {
                    $('.footer__title').removeClass('active').next().slideUp();
                    $(this).toggleClass('active').next().slideDown();
                }
                return false;
            });
        }
        else{
            $('.footer__nav').slideDown(0);
        }
    }
    



    $(window).resize(function(){
        init_accordeon();
    })


    // if (screen.orientation.lock){
    //     screen.orientation.lock("landscape");
    //     screen.lockOrientation("landscape");
    //     console.log('dsd');
        
    // } else {
    //     alert('Переверните устройство в горизонтальное положение');
    // }



    // function ready() {
    //     const {
    //         type
    //     } = screen.orientation;
    //     console.log(`Fullscreen and locked to ${type}. Ready!`);
    // }

    // async function start() {
    //     await document.body.requestFullscreen();
    //     await screen.orientation.lock("landscape");
    //     await  screen.lockOrientation("landscape");
    //     ready();
    // };

    // setTimeout(() => {
    //     start();
    //     console.log('sfff');
        
    // }, 500);
    // card wrapper
    // var $container = document.getElementById('deck_start');

    // // create Deck
    // var deck = Deck();

    // // add to DOM
    // deck.mount($container);

    // deck.cards.forEach(function (card, i) {
    //     card.setSide('back');
    //     // card.enableDragging();
    //     card.enableFlipping();
        
    //     // explode
    //     // card.animateTo({
    //     //     delay: 1000 + i * 2, // wait 1 second + i * 2 ms
    //     //     duration: 500,
    //     //     ease: 'quartOut',

    //     //     x: Math.random() * window.innerWidth - window.innerWidth / 2,
    //     //     y: Math.random() * window.innerHeight - window.innerHeight / 2
    //     // });
    // });

    // //start game
    // deck.shuffle();
    // deck.shuffle();

    // var gamer1_deck = $('.gamer__cards')[0];
    // var gamer1_position = gamer1_deck.getBoundingClientRect();
    // var g1_left = gamer1_position.left;
    // var g1_top = gamer1_position.top;
    // // console.log(g1_left);
    // // console.log(g1_top);
    // var players = [];

    // deck.durak(g1_left, g1_top);

    // deck.cards[14].
});