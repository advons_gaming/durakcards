﻿

function onReceive(com, error, payload)
{
    switch (com)
    {
        case ClientCommand.Auth:
            if (error == Error.Success)
            {
                //updateAccountInfo();
               // updateBalance();
                //updateListTables();
            }

            break;

        case ClientCommand.CreateTable:
        case ClientCommand.OpenTable:
            if (error == Error.Success)
            {
                for (var i = 0; i < table.players.length; i++)
                {
                    game.usrTable.addGamer(table.players[i].id, table.players[i].login)
                }
                //updateSeats();
            }

            break;

        case ClientCommand.CloseTable:
            //onCloseTable();
            break;
        case ClientCommand.AddPlayer:
            game.usrTable.clearList()
            for (var i = 0; i < table.players.length; i++) {
                game.usrTable.addGamer(table.players[i].id, table.players[i].login)
            }
        case ClientCommand.ErasePlayer:
            //updateSeat(payload);
            break;

        case ClientCommand.WaitPlayersConfirm:
            //document.getElementById("btnConfirm").style.display = "block";
            break;

        case ClientCommand.PlayerConfirm:
            game.usrTable.userReady(payload)
            break;

        case ClientCommand.StartGame:
            game.usrTable.hide()
            game.view.createDeck()

            break;

        case ClientCommand.NewRaund:
            game.view.cardsCount.text = table.deckCardsCount

            game.view.updateSeats();
            game.view.updateTableCards();
            game.view.updatePlayersCards();
            //updatePlayersState();
            game.view.startTimerWaitMove();

            break;

        case ClientCommand.SetPlayerCards:
            if (table.playerAttack == account.id) game.view.updateCardsAttack();
            else game.view.updateCards();
            break;

        case ClientCommand.Attack:
            if (error == Error.Success)
            {
                game.view.updateTableCards()
                game.view.updatePlayersCards()
                game.view.startTimerWaitMove();
                if (table.state == GameState.WaitAttack && table.playerAttack == account.id) game.view.updateCardsAttack();
                else if (table.state == GameState.WaitDefending && table.playerDefending == account.id) game.view.updateCardsDefending();
                else game.view.updateCards();
            }
            break;

        case ClientCommand.Defending:
            if (error == Error.Success)
            {
				game.view.updateTableCards()
				game.view.updatePlayersCards()
                game.view.startTimerWaitMove();
                if (table.state == GameState.WaitAttack && table.playerAttack == account.id) game.view.updateCardsAttack();
                else game.view.updateCards();
            }
            break;

        case ClientCommand.AttackEnd:
            game.view.updateTableCards();
            game.view.startTimerWaitMove();
            if (table.state == GameState.WaitAttack && table.playerAttack == account.id) game.view.updateCardsAttack();
            else game.view.updateCards();
            //updatePlayersState();
            break;

        case ClientCommand.GiveUp:
            game.view.updateTableCards();
            game.view.startTimerWaitMove();
            if (table.state == GameState.WaitAttack && table.playerAttack == account.id) game.view.updateCardsAttack();
            else game.view.updateCards();
            break;

        case ClientCommand.EndGame:
            game.view.updateSeats();
            game.view.updateTableCards();
            game.view.stopTimerWaitMove();
            break;

        case ClientCommand.TechnicalDefeat:
            game.view.stopTimerWaitMove();
            break;

        case ClientCommand.Win:
            {
                if (payload.accountId == account.id) {
                    game.finishPopup.showPopup()
                    game.view.win()
                }
                // let seatidx = table.getPlayerIdx(payload.accountId);
                // document.getElementById("win" + seatidx).innerHTML = "+" + payload.winSumm;
                // document.getElementById("playerState" + seatidx).innerHTML = "WIN";
                // console.log(payload);
            }
            break;

        case ClientCommand.Lose:
            if (payload.accountId == account.id) {
                game.finishPopup.showPopup()
                game.view.lose()
            }
            // let seatidx = table.getPlayerIdx(payload.accountId);
            // document.getElementById("win" + seatidx).innerHTML = "-" + payload.loseSumm;
            // document.getElementById("playerState" + seatidx).innerHTML = "LOSE";
            // console.log(payload);
            break;

        case ClientCommand.UpdateTableList:
            //updateListTables();
            break;

        case ClientCommand.UpdateBalance:
            //updateBalance();
            break;
    }
}