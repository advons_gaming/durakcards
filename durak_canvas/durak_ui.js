﻿var timerWaitMove = 0, timerWaitMoveSec = 0;

function cardToString(card)
{
    var str = '';

    if (card.rank <= CARD_RANK._10) str += card.rank + 2;
    else if (card.rank == CARD_RANK.J) str += "J";
    else if (card.rank == CARD_RANK.Q) str += "Q";
    else if (card.rank == CARD_RANK.K) str += "K";
    else if (card.rank == CARD_RANK.A) str += "A";

    if (card.suit == CARD_SUIT.SPADES) str += "♠";
    else if (card.suit == CARD_SUIT.CLUBS) str += "♣";
    else if (card.suit == CARD_SUIT.DIAMONDS) str += "♦";
    else if (card.suit == CARD_SUIT.HEARTS) str += "♥";

    return str;
}

function cardToSpan(card)
{
    var str = '<span style="display:inline-block;font-size:24px;border:1px solid ';
    if (card.suit == table.trumpCard.suit) str += 'orange';
    else str += 'black';
    str += ';padding:5px;color:';

    if (card.suit == CARD_SUIT.SPADES || card.suit == CARD_SUIT.CLUBS) str += 'black';
    else str += 'red';

    str += '">' + cardToString(card) + '</span>';

    return str;
}

function existRank(cards, rank)
{
    for (let i = 0; i < cards.length; ++i)
        if (cards[i].rank == rank) return true;

    return false;
}

function onCloseConnection()
{
    document.getElementById("lobby").style.display = "none";
    document.getElementById("table").style.display = "none";
    document.getElementById("lostConnection").style.display = "block";
}

function onReceive(com, error, payload)
{
    switch (com)
    {
        case ClientCommand.Auth:
            if (error == Error.Success)
            {
                //updateAccountInfo();
                //updateBalance();
                updateListTables();
            }

            break;

        case ClientCommand.CreateTable:
        case ClientCommand.OpenTable:
            if (error == Error.Success)
            {
                document.getElementById("lobby").style.display = "none";
                document.getElementById("table").style.display = "block";

                var html = '<tr><th>Seat</th><th>Id</th><th>Login</th><th>Avatar</th><th>Cards</th><th>Timer</th><th>State</th><th>Win</th></tr>';
                for (var i = 0; i < table.players.length; i++)
                {
                    game.usrTable.addGamer(table.players[i].id, table.players[i].login)
                    html += '<tr><td>' + i + '</td><td id="playerId' + i + '"></td><td id="login' + i + '"></td><td id="avatar' + i + '"></td><td id="cards' + i + '"></td><td id="timer' + i + '"></td><td id="playerState' + i + '"></td><td id="win' + i + '"></td></tr>';
                }
                document.getElementById("players").innerHTML = html;

                updateSeats();
            }

            break;

        case ClientCommand.CloseTable:
            onCloseTable();
            break;
        case ClientCommand.AddPlayer:
            game.usrTable.clearList()
            for (var i = 0; i < table.players.length; i++) {
                game.usrTable.addGamer(table.players[i].id, table.players[i].login)
            }
        case ClientCommand.ErasePlayer:
            updateSeat(payload);
            break;

        case ClientCommand.WaitPlayersConfirm:
            //document.getElementById("btnConfirm").style.display = "block";
            break;

        case ClientCommand.PlayerConfirm:
            console.log(payload);
            game.usrTable.userReady(payload)
            break;

        case ClientCommand.StartGame:
            game.usrTable.hide()
            game.view.createDeck()

            break;

        case ClientCommand.NewRaund:
            game.view.cardsCount.text = table.deckCardsCount
            document.getElementById('deckCardsCount').innerHTML = table.deckCardsCount + ' / ' + table.deckSize;

            game.view.updateSeats();
            game.view.updateTableCards();
            game.view.updatePlayersCards();

            updateSeats();
           // updateTableCards();
            updatePlayersCards();
            updatePlayersState();
            startTimerWaitMove();
            game.view.startTimerWaitMove();

            break;

        case ClientCommand.SetPlayerCards:
            if (table.playerAttack == account.id) game.view.updateCardsAttack();
            else game.view.updateCards();
            break;

        case ClientCommand.Attack:
            if (error == Error.Success)
            {
                game.view.updateTableCards()
                game.view.updatePlayersCards()
                updateTableCards();
                updatePlayersCards();
                startTimerWaitMove();
                game.view.startTimerWaitMove();
                if (table.state == GameState.WaitAttack && table.playerAttack == account.id) game.view.updateCardsAttack();
                else if (table.state == GameState.WaitDefending && table.playerDefending == account.id) game.view.updateCardsDefending();
                else game.view.updateCards();
            }
            break;

        case ClientCommand.Defending:
            if (error == Error.Success)
            {
				game.view.updateTableCards()
				game.view.updatePlayersCards()
                updateTableCards();
                updatePlayersCards();
                startTimerWaitMove();
                game.view.startTimerWaitMove();
                if (table.state == GameState.WaitAttack && table.playerAttack == account.id) game.view.updateCardsAttack();
                else game.view.updateCards();
            }
            break;

        case ClientCommand.AttackEnd:
            game.view.updateTableCards();
            updateTableCards();
            startTimerWaitMove();
            game.view.startTimerWaitMove();
            if (table.state == GameState.WaitAttack && table.playerAttack == account.id) game.view.updateCardsAttack();
            else game.view.updateCards();
            updatePlayersState();
            break;

        case ClientCommand.GiveUp:
            game.view.updateTableCards();
            updateTableCards();
            startTimerWaitMove();
            game.view.startTimerWaitMove();
            if (table.state == GameState.WaitAttack && table.playerAttack == account.id) game.view.updateCardsAttack();
            else game.view.updateCards();
            break;

        case ClientCommand.EndGame:
            game.view.updateSeats();
            game.view.updateTableCards();
            game.view.stopTimerWaitMove();

            // updateSeats();
            // updateTableCards();
            // stopTimerWaitMove();
            break;

        case ClientCommand.TechnicalDefeat:
            game.view.stopTimerWaitMove();
            stopTimerWaitMove();
            break;

        case ClientCommand.Win:
            {
                if (payload.accountId == account.id)
                    game.view.win()
                // let seatidx = table.getPlayerIdx(payload.accountId);
                // document.getElementById("win" + seatidx).innerHTML = "+" + payload.winSumm;
                // document.getElementById("playerState" + seatidx).innerHTML = "WIN";
                // console.log(payload);
            }
            break;

        case ClientCommand.Lose:
            if (payload.accountId == account.id)
                game.view.lose()
            // let seatidx = table.getPlayerIdx(payload.accountId);
            // document.getElementById("win" + seatidx).innerHTML = "-" + payload.loseSumm;
            // document.getElementById("playerState" + seatidx).innerHTML = "LOSE";
            // console.log(payload);
            break;

        case ClientCommand.UpdateTableList:
            updateListTables();
            break;

        case ClientCommand.UpdateBalance:
            updateBalance();
            break;
    }
}


function onTechnicalDefeat(seatIdx)
{
    updateSeat(seatIdx);
}

function updateAccountInfo()
{
    //document.getElementById("accountInfo").innerHTML = '<img src="https://steam-games.net/' + account.avatar + '" width="48" style="float:left;margin-right:10px;" /> ID: ' + account.id + ' ' + account.login;
}

function updateBalance()
{
    //document.getElementById("balance").innerHTML = "bronze: " + account.balance[0] + " | silver: " + account.balance[1] + " | gold: " + account.balance[2];
}

var game_speed = {'20': 'быстрая', '30': 'нормальная', '40': 'медленная'}
function getSpeedLabel(val) {
    if (game_speed[val]) {
        return game_speed[val]
    }
    return val
}

function sendOpenTableFromUi(id) {
    window.localStorage.setItem('durak_tableId', id)
    document.location.href = 'pageGameProcess.html'

}

function updateListTables()
{

    var html = ''

    for (var i = 0; i < tables.length; i++)
    {
        var myvar = '<li class="games-list__table-item selected">'+
        '<a onclick="sendOpenTableFromUi(' + tables[i].id + ')" class="games-list__table-item__inner">'+
        '<div class="games-list__table-item-name">'+
        '    Игровой стол ' + tables[i].id+
        '</div>'+
        '<div class="games-list__table-item-block">'+
        '<span>1000</span>'+
        '<figure class="history__right-icon"><img srcset="../img/blocks/shop/icon--coins-gold-cutted.png" alt=""></figure>'+
        '</div>'+
        '<div class="games-list__table-item-block">'+
        '    ' + tables[i].playersCount + ' / ' + tables[i].seatsCount +
        '</div>'+
        '<div class="games-list__table-item-block">'+
        '    ' + getSpeedLabel(tables[i].gameSpeed) +
        '</div>'+
        '</a>                            '+
        '</li>';
    
        html += myvar

        //'<tr><td>' + tables[i].id + '</td><td>' + tables[i].playersCount + ' / ' + tables[i].seatsCount + '</td><td>' + tables[i].gameSpeed + '</td><td>' + tables[i].deckSize + '</td><td><input type="button" id="btnOpenTable" value="Open" onclick="sendOpenTable(' + tables[i].id + ')" /></td></tr>';
    }
    document.getElementsByClassName("games-list__table")[0].innerHTML = html;
}

function updateSeat(seatIdx)
{
    if (table.players[seatIdx].id)
    {
        //document.getElementById("playerId" + seatIdx).innerHTML = table.players[seatIdx].id;
        //document.getElementById("login" + seatIdx).innerHTML = table.players[seatIdx].login;
       // document.getElementById("avatar" + seatIdx).innerHTML = '<img src="https://steam-games.net/' + table.players[seatIdx].avatar + '" width="32" />';
    }
    else
    {
        document.getElementById("playerId" + seatIdx).innerHTML = "";
        document.getElementById("login" + seatIdx).innerHTML = "";
        document.getElementById("avatar" + seatIdx).innerHTML = "";
        document.getElementById("cards" + seatIdx).innerHTML = "";
        document.getElementById("playerState" + seatIdx).innerHTML = "";
        document.getElementById("win" + seatIdx).innerHTML = "";
    }
}

function updateSeats()
{
    for (let i = 0; i < table.players.length; ++i)
        updateSeat(i);
}

function updateTableCards()
{   

    let html = '';
    for (let i = 0; i < table.cards.length; ++i)
    {
        html += cardToSpan(table.cards[i]) + ' ';

    }

    // var trmp = table.trumpCard

    // game.stateManager.trmpCard(trmp)

    document.getElementById("tableCards").innerHTML = html;

    
}


function updateCardsAttack()
{
    let html = '';
    for (let i = 0; i < table.playerCards.length; ++i)
    {
        let card = table.playerCards[i];

        if (table.cards.length == 0 || existRank(table.cards, card.rank))
        {
            html += '<input type="button" style="border:1px solid ';
            if (card.suit == table.trumpCard.suit) html += 'orange';
            else html += 'black';
            html += ';font-size:24px;color:';

            if (card.suit == CARD_SUIT.SPADES || card.suit == CARD_SUIT.CLUBS) html += 'black';
            else html += 'red';

            html += '" value="' + cardToString(card) + '" onclick="sendAttack(' + card.rank + ', ' + card.suit + ')" /> ';

        }

        else html += cardToSpan(card) + ' ';
    }

    document.getElementById("cards" + table.accountIdx).innerHTML = html;

    if (table.cards.length > 0) document.getElementById("btnAttackEnd").style.display = "block";
    else document.getElementById("btnAttackEnd").style.display = "none";
    document.getElementById("btnGiveUp").style.display = "none";
}

function updateCardsDefending()
{
    let html = '';
    for (let i = 0; i < table.playerCards.length; ++i)
    {
        let card = table.playerCards[i];

        if ((card.suit != table.trumpCard.suit && card.suit == table.cards[table.cards.length - 1].suit && card.rank > table.cards[table.cards.length - 1].rank) || card.suit == table.trumpCard.suit)
        {
            html += '<input type="button" style="border:1px solid ';
            if (card.suit == table.trumpCard.suit) html += 'orange';
            else html += 'black';
            html += ';font-size:24px;color:';

            if (card.suit == CARD_SUIT.SPADES || card.suit == CARD_SUIT.CLUBS) html += 'black';
            else html += 'red';

            html += '" value="' + cardToString(card) + '" onclick="sendDefending(' + card.rank + ', ' + card.suit + ')" /> ';
        }
        else html += cardToSpan(card) + ' ';
    }

    document.getElementById("cards" + table.accountIdx).innerHTML = html;

    document.getElementById("btnAttackEnd").style.display = "none";
    document.getElementById("btnGiveUp").style.display = "block";
}

function updatePlayersCards()
{
    for (let i = 0; i < table.players.length; ++i)
    {
        if (table.players[i].id != 0)
        {
            if (table.players[i].id != account.id)
            {
                document.getElementById("cards" + i).innerHTML = '';
                for (let j = 0; j < table.players[i].cardsCount; ++j)
                {
                    document.getElementById("cards" + i).innerHTML += '<span style="display:inline-block;font-size:24px;border:1px solid black;padding:5px;">X</span> ';
                }
            }
            else document.getElementById("cards" + i).innerHTML = "";
        }
        else document.getElementById("cards" + i).innerHTML = "";
    }
}

function updatePlayersState()
{
    for (var i = 0; i < table.players.length; i++)
    {
        if (table.players[i].id != 0)
        {
            if (table.playerAttack == table.players[i].id) document.getElementById("playerState" + i).innerHTML = 'Attack';
            else if (table.playerDefending == table.players[i].id) document.getElementById("playerState" + i).innerHTML = 'Defending';
            else document.getElementById("playerState" + i).innerHTML = '';
        }
        else document.getElementById("playerState" + i).innerHTML = '';
    }
}

function startTimerWaitMove()
{
    stopTimerWaitMove();
    timerBetActionSec = table.gameSpeed;

    if (table.state == GameState.WaitAttack) document.getElementById("timer" + table.getPlayerIdx(table.playerAttack)).innerHTML = timerBetActionSec;
    else if (table.state == GameState.WaitDefending) document.getElementById("timer" + table.getPlayerIdx(table.playerDefending)).innerHTML = timerBetActionSec;

    timerWaitMove = setInterval(function ()
    {
        timerBetActionSec--;

        if (table.state == GameState.WaitAttack) document.getElementById("timer" + table.getPlayerIdx(table.playerAttack)).innerHTML = timerBetActionSec;
        else if (table.state == GameState.WaitDefending) document.getElementById("timer" + table.getPlayerIdx(table.playerDefending)).innerHTML = timerBetActionSec;

        if (timerBetActionSec < 0) stopTimerWaitMove();
    }, 1000);
}

function stopTimerWaitMove()
{
    clearInterval(timerWaitMove);
    timerWaitMove = 0;

    for (var i = 0; i < table.players.length; i++)
    {
        document.getElementById("timer" + i).innerHTML = "";
    }
}

function onCloseTable()
{
    document.getElementById("lobby").style.display = "block";
    document.getElementById("table").style.display = "none";
    stopTimerWaitMove();
}
