var ClientWS = function()
{
    this.Socket = null;
    this.onOpen = null;
    this.onClose = null;
    this.onMessage = null;
    this.onError = null;
    this.com = new Serialize();

    var _this = this;

    if (window.WebSocket === undefined) alert("sockets not supported");

    this.Connect = function (server_ip, server_port, ssl)
    {
        var str = "ws";
        if (ssl == true) str = "wss";
        this.Socket = new WebSocket(str + "://" + server_ip + ":" + server_port + "/");
        this.Socket.binaryType = "arraybuffer";
        this.Socket.onopen = this.onOpen;
        this.Socket.onmessage = this._onMessage;
        this.Socket.onclose = this.onClose;
        this.Socket.onerror = this._onError;
    };

    this.Close = function ()
    {
        if (this.Socket != null && this.isConnected()) this.Socket.close();
    };

    this._onMessage = function (event)
    {
        if (event.data instanceof ArrayBuffer)
        {
            var serialize = new Serialize();
            serialize.Buffer = event.data;
            serialize.BufferPnt = 0;
            serialize.BufferSize = event.data.byteLength;
            _this.onMessage(serialize);
        }
    };

    this._onError = function(event)
    {
        var reason;
        if (event.code == 1000)
            reason = "Normal closure, meaning that the purpose for which the connection was established has been fulfilled.";
        else if (event.code == 1001)
            reason = "An endpoint is \"going away\", such as a server going down or a browser having navigated away from a page.";
        else if (event.code == 1002)
            reason = "An endpoint is terminating the connection due to a protocol error";
        else if (event.code == 1003)
            reason = "An endpoint is terminating the connection because it has received a type of data it cannot accept (e.g., an endpoint that understands only text data MAY send this if it receives a binary message).";
        else if (event.code == 1004)
            reason = "Reserved. The specific meaning might be defined in the future.";
        else if (event.code == 1005)
            reason = "No status code was actually present.";
        else if (event.code == 1006)
            reason = "The connection was closed abnormally, e.g., without sending or receiving a Close control frame";
        else if (event.code == 1007)
            reason = "An endpoint is terminating the connection because it has received data within a message that was not consistent with the type of the message (e.g., non-UTF-8 [http://tools.ietf.org/html/rfc3629] data within a text message).";
        else if (event.code == 1008)
            reason = "An endpoint is terminating the connection because it has received a message that \"violates its policy\". This reason is given either if there is no other sutible reason, or if there is a need to hide specific details about the policy.";
        else if (event.code == 1009)
            reason = "An endpoint is terminating the connection because it has received a message that is too big for it to process.";
        else if (event.code == 1010) // Note that this status code is not used by the server, because it can fail the WebSocket handshake instead.
            reason = "An endpoint (client) is terminating the connection because it has expected the server to negotiate one or more extension, but the server didn't return them in the response message of the WebSocket handshake. <br /> Specifically, the extensions that are needed are: " + event.reason;
        else if (event.code == 1011)
            reason = "A server is terminating the connection because it encountered an unexpected condition that prevented it from fulfilling the request.";
        else if (event.code == 1015)
            reason = "The connection was closed due to a failure to perform a TLS handshake (e.g., the server certificate can't be verified).";
        else
            reason = "Unknown reason";

        event.reason = reason;
        _this.onError(event);
    }


    this.Send = function()
    {
        if (this.isConnected()) this.Socket.send(this.com.Buffer);
        this.com.Clear();
    }

    this.isConnected = function()
    {
        if (this.Socket.readyState === this.Socket.OPEN) return true;
        return false;
    };

};