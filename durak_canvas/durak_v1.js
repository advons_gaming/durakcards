var utils = (function() {
    "use strict";
    function o(e) {
            return (o = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function(e) {
                return typeof e
            } : function(e) {
                return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e
            })(e)
        }

    var lib = {
        PIXEL_RATIO: (function() {
            var ctx = document.createElement("canvas").getContext("2d"),
                dpr = window.devicePixelRatio || 1,
                bsr = ctx.webkitBackingStorePixelRatio ||
                ctx.mozBackingStorePixelRatio ||
                ctx.msBackingStorePixelRatio ||
                ctx.oBackingStorePixelRatio ||
                ctx.backingStorePixelRatio || 1;
                return dpr / bsr;
        })(),
        mixin: function(target, source) {
            if (source) {
                for (var key, keys = Object.keys(source), l = keys.length; l--; ) {
                    key = keys[l];

                    if (source.hasOwnProperty(key)) {
                        target[key] = source[key];
                    }
                }
            }
            return target;
        },
        beautify_number: function(num, delimiter) {
            delimiter = (delimiter == void 0 && ' ') || delimiter
            return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, delimiter);
        },
        intToString: function(value) {
            if (value >= 1000) {
                var suffixes = ["", "K", "M", "B", "T"];
                var suffixNum = Math.floor((""+value).length/3);
                var shortValue = parseFloat((suffixNum != 0 ? (value / Math.pow(1000,suffixNum)) : value).toPrecision(3));
                if (shortValue % 1 != 0) {
                    var shortNum = shortValue.toFixed(1);
                }
                return shortValue+suffixes[suffixNum];
            } else
                return value;
        },
        randomRanges: function(min, max) {
            return min + (Math.random() * (max - min));
        },
        randomInt: function(min, max) {
            return min + Math.floor(Math.random() * (max - min + 1));
        },
        getSpriteFromG: function(bound, color, line, type, points) {
            var graphics = game.add.graphics(0, 0);
            bound.width = (bound.width == void 0 && 100) || bound.width, bound.height = (bound.height == void 0 && 100) || bound.height, bound.radius = (bound.radius == void 0 && 100) || bound.radius
            color = (color == void 0 && {c: 0x009b6b, o: 1}) || color, color.o = (color.o == void 0 && 1) || color.o, color.c = (color.c == void 0 && 0x009b6b) || color.c
            graphics.beginFill(color.c, color.o);
            if (line) {
                line.w = (line.w == void 0 && 3) || line.w, line.o = (line.o == void 0 && 1) || line.o
                graphics.lineStyle(line.w, line.c, line.o);
            }
            if (type == 'poly') {
                points = (points == void 0 && [[0, 0],[100, 0],[100, 100],[0, 100], [0, 0]]) || points
                graphics.moveTo(points[0][0],points[0][1]);
                for (var i = 1; i < points.length; i++)
                    graphics.lineTo(points[i][0], points[i][1]);
            } else {
                type == 'circle' ? graphics.drawCircle(0, 0, bound.radius) : type == 'rounded' ? graphics.drawRoundedRect(0, 0, bound.width, bound.height, bound.radius) : graphics.drawRect(0, 0, bound.width, bound.height);
            }
            graphics.endFill();
            var sprite = game.make.sprite(bound.x, bound.y, graphics.generateTexture());
            graphics.destroy()
            return sprite
        },
        is: function(percent) {
            percent = percent || 50
            if (this.randomInt(1,100) <= percent)
                return true
            else
                return false
        },
        getSecondsFrom: function(time) {
            return ((new Date().getTime() - time) / 1000) + 's.'
        },
        contains: function(x, y, obj) {
            if (x > obj.x &&
                x < obj.x + obj.width &&
                y > obj.y &&
                y < obj.y + obj.height
            )
                return true;
            return false;
        },
        sizeFromNum: function(defaultSize, number) {
            return number > 999999 ? defaultSize * 0.38 : number > 99999 ? defaultSize * 0.46 : number > 9999 ? defaultSize * 0.56 : number > 999 ? defaultSize * 0.7 : number > 99 ? defaultSize * 0.9 : defaultSize
        },
        sizeFromLength: function(defaultSize, text, defaultLength) {
            var curr_length = text.length
            if (curr_length < defaultLength)
                return defaultSize
            return Math.floor((Math.tanh(defaultLength / curr_length)) * defaultSize);
        },
        centerGameObjects: function(e) {
            e.forEach(function(e) {
                e.anchor.setTo(.5)
            })
        },
        setPositionToGameCenter: function(e) {
            e.forEach(function(e) {
                e.position.set(config.GAME_CENTER_X, config.GAME_CENTER_Y)
            })
        },
        setInputPriority: function(e, t) {
            e && (e.input || (e.inputEnabled = true), e.input.priorityID = t)
        },
        PL: function(e, t) {
            return config.isLandscape ? t : e
        },
        makeDraggableGroup: function(e) {
            function i(e) {
                if (e.forEach) e.forEach(i);
                else {
                    e.inputEnabled = true, e.input.enableDrag(), l.setInputPriority(e, 100), e.origin = new Phaser.Point(e.x, e.y);
                    var t = n.forEach ? a : r;
                    t && e.events.onDragUpdate.add(t), o && e.events.onDragStart.add(o), s && e.events.onDragStop.add(s)
                }
            }

            function a(e, t, i, a, o, s) {
                s && (e.startDragPos = new Phaser.Point(e.x, e.y), n.startDragPos = new Phaser.Point(n.x, n.y)), n.x = n.startDragPos.x - e.origin.x + i / game.resizeMan.scaleFactor, n.y = n.startDragPos.y - e.origin.y + a / game.resizeMan.scaleFactor, e.x = e.startDragPos.x, e.y = e.startDragPos.y, r && r(e, t, i, a, o, s)
            }
            var n = e.item,
                o = e.startCallback,
                s = e.stopCallback,
                r = e.updateCallback;
            n.forEach ? n.forEach(i) : i(n)
        },
        makeDraggableSprite: function(e) {
            var t = e.item,
                i = e.startCallback,
                a = e.stopCallback,
                n = e.updateCallback;
            t.inputEnabled = true, t.input.enableDrag(), l.setInputPriority(t, 100), t.origin = new Phaser.Point(t.x, t.y), t.events.onDragUpdate.add(function(e, t, i, a, o, s) {
                s && (e.startWorldPos = new Phaser.Point(e.world.x, e.world.y), e.startDragPos = new Phaser.Point(e.x, e.y)), n && n(e, t, i, a, o, s)
            }), i && t.events.onDragStart.add(i), a && t.events.onDragStop.add(a)
        },
        makeDraggableWithParamsDebug: function(e) {
            e instanceof Phaser.Group ? l.makeDraggableGroup({
                item: e,
                startCallback: function(e) {
                    game.debugMan.addToDebugGuiCommon(e)
                },
                updateCallback: function(e, t, i, a, o, s) {
                    l.log("Parent position x: ".concat(e.parent.position.x, " y: ").concat(e.parent.position.y)), l.log("Parent scale x: ".concat(e.scale.x, " y: ").concat(e.scale.y))
                }
            }) : l.makeDraggableSprite({
                item: e,
                startCallback: function(e) {
                    game.debugMan.addToDebugGuiCommon(e)
                },
                updateCallback: function(e, t, i, a, o, s) {
                    l.log("Object position x: ".concat(e.position.x, " y: ").concat(e.position.y)), l.log("Object scale x: ".concat(e.scale.x, " y: ").concat(e.scale.y))
                }
            })
        },
        clone: function(e) {
            if (null == e || "object" != (t = e, ("function" == typeof Symbol && "symbol" === o(Symbol.iterator) ? function(e) {
                    return o(e)
                } : function(e) {
                    return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : o(e)
                })(t))) return e;
            var t, i = e.constructor();
            for (var a in e) e.hasOwnProperty(a) && (i[a] = e[a]);
            return i
        },
        cloneObject: function(e) {
            var t;
            if (e instanceof Object) {
                for (var i in t = {}, e) e.hasOwnProperty(i) && (t[i] = this.clone(e[i]));
                return t
            }
        },
        distance: function(x1, y1, x2, y2) {
            var a = x1 - x2;
            var b = y1 - y2;
            return Math.sqrt( a*a + b*b );
        },
        getPosInMainGroup: function(e) {
            var t = new Phaser.Point(e.x * e.parent.scale.x, e.y * e.parent.scale.y);
            if (e !== game.mainGroup) {
                var i = this.getPosInMainGroup(e.parent);
                t.x += i.x, t.y += i.y
            } else t.x = 0, t.y = 0;
            return t
        },
        doNothing: function() {},
        throttle: function(a, o, s) {
            var n, r;
            return o || (o = 250),
                function() {
                    var e = s || this,
                        t = +new Date,
                        i = arguments;
                    n && t < n + o ? (clearTimeout(r), r = setTimeout(function() {
                        n = t
                    }, o)) : (n = t, a.apply(e, i))
                }
        }
    };
    return lib;
})();

var Loader = (function() {
    var manager = {
        cbs: [],
        init: function() {
            game.load.onFileComplete.add(this.fileComplete, this);
           // game.load.onLoadComplete.add(this.loadComplete, this);
        },
        fileComplete: function(progress, cacheKey, success, totalLoaded, totalFiles) {
            this.cbs[cacheKey]()
            this.cbs[cacheKey] = void 0
        },
        load: function(id, src, cb) {
            this.cbs[id] = cb
            game.load.image(id, src);
            game.load.start();
        }

    }
    manager.init()
    return manager
})

var FinishPopUp = (function () {
    "use strict";
    var FINISH = {
        showPopup: function() {
            this.background.visible = this.dark.visible = this.btnExit.inputEnabled = this.btnContinue.inputEnabled = true;
        },
        createFinish: function () {
            this.dark = game.add.graphics(0,0);
            this.dark.beginFill(0x000000, 0.75);
            this.dark.drawRect(0,0, game.world.width, game.world.height);
            this.dark.inputEnabled = true
            this.dark.visible = false

            this.backgroundg = game.add.graphics(0, 0)
            this.backgroundg.beginFill(0x0D374F, 0.75)
            this.backgroundg.drawRoundedRect(0, 0, 477, 300, 5)
            this.backgroundg.endFill()

            this.backgroundTop = game.add.graphics(0, 0)
            this.backgroundTop.beginFill(0x2190B8, 0.75)
            this.backgroundTop.drawRoundedRect(0, 0, 477, 40, 5)
            this.backgroundTop.endFill()

            this.background = game.add.sprite(game.world.centerX, game.world.centerY, this.backgroundg.generateTexture());
            this.background.anchor.set(0.5);
            this.backgroundg.destroy();

            this.top = game.add.sprite(0, -129, this.backgroundTop.generateTexture())
            this.top.anchor.set(0.5);
            this.backgroundTop.destroy();
            this.background.addChild(this.top);

            this.topFrame = game.add.sprite(-4, -2, 'modal-shape_head');
            this.topFrame.anchor.set(0.5);
            this.top.addChild(this.topFrame);

            this.topName = game.add.text(0, 0, "ИТОГИ ИГРЫ");
            this.topName.anchor.set(0.4, 0.5);
            this.topName.scale.set(0.5)
            this.topName.fontFamily = 'arial';
            this.topName.fontWeight = 700;
            this.topName.fill = "#FFFFFF";
            this.topName.fontSize = 46
            this.top.addChild(this.topName);

            this.placeFrame = game.add.sprite(-232, -105, 'modal_shape');
            this.placeFrame.scale.set(0.35, 1)
            this.background.addChild(this.placeFrame);

           this.placeName = game.add.text(55, 6, 'МЕСТО');
            this.placeName.scale.set(1, 0.4);
            this.placeName.fontFamily = 'arial';
            this.placeName.fontWeight = 'bold';
            this.placeName.fill = "#FFFFFF";
            this.placeName.fontSize = 45;
            this.placeFrame.addChild(this.placeName);

            this.gamerFrame = game.add.sprite(-136, -105, 'modal_shape');
            this.gamerFrame.scale.set(0.5, 1)
            this.background.addChild(this.gamerFrame);

            this.gamerName = game.add.text(55, 6, 'ИГРОКИ');
            this.gamerName.scale.set(1, 0.55);
            this.gamerName.fontFamily = 'arial';
            this.gamerName.fontWeight = 'bold';
            this.gamerName.fill = "#FFFFFF";
            this.gamerName.fontSize = 34;
            this.gamerFrame.addChild(this.gamerName);

            this.coinFrame = game.add.sprite(0, -105, 'modal_shape');
            this.coinFrame.scale.set(0.43, 1)
            this.background.addChild(this.coinFrame);

            this.coinName = game.add.text(55, 6, 'PFP COIN');
            this.coinName.scale.set(1, 0.5);
            this.coinName.fontFamily = 'arial';
            this.coinName.fontWeight = 'bold';
            this.coinName.fill = "#FFFFFF";
            this.coinName.fontSize = 35;
            this.coinFrame.addChild(this.coinName);

            this.totalFrame = game.add.sprite(118, -105, 'modal_shape');
            this.totalFrame.scale.set(0.43, 1)
            this.background.addChild(this.totalFrame);

            this.totalName = game.add.text(55, 6, 'РЕЙТИНГ');
            this.totalName.scale.set(1, 0.5);
            this.totalName.fontFamily = 'arial';
            this.totalName.fontWeight = 'bold';
            this.totalName.fill = "#FFFFFF";
            this.totalName.fontSize = 35;
            this.totalFrame.addChild(this.totalName);

            this.tableGame = game.add.sprite(0, 35, 'table-shapef');
            this.tableGame.anchor.set(0.5)
            //this.tableGame.scale.set(2,2)
            this.background.addChild(this.tableGame);
         
            this.btnExit = game.add.sprite(-120, 122, 'blueBtn');
            this.btnExit.anchor.set(0.5)
            this.background.addChild(this.btnExit);

            this.btnExitName = game.add.text(0, 0, 'ВЫЙТИ');
            this.btnExitName.anchor.set(0.5)
            this.btnExitName.scale.set(0.4)
            this.btnExitName.font = '"Open Sans", Helvetica, Arial, sans-serif';
            this.btnExitName.fontWeight = 'bold';
            this.btnExitName.fill = "#dfe2dd";
            this.btnExitName.fontSize = 36;
            this.btnExit.addChild(this.btnExitName);

            this.btnExitRedName = game.add.text(0, 0, 'ВЫЙТИ');
            this.btnExitRedName.anchor.set(0.5)
            this.btnExitRedName.scale.set(0.4)
            this.btnExitRedName.font = '"Open Sans", Helvetica, Arial, sans-serif';
            this.btnExitRedName.fontWeight = 'bold';
            this.btnExitRedName.fill = "#ad0600";
            this.btnExitRedName.fontSize = 36;
            this.btnExitRedName.alpha = 0;
            this.btnExit.addChild(this.btnExitRedName);

            this.btnExit.inputEnabled = true;
            this.btnExit.events.onInputOver.add(function () {
                game.add.tween(this.btnExitName).to({
                    alpha: 0
                }, 200, "Linear", true)
                var show = game.add.tween(this.btnExitRedName).to({
                    alpha: 1
                }, 200, "Linear", true)
            }, this);

            this.btnExit.events.onInputOut.add(function () {
                game.add.tween(this.btnExitName).to({
                    alpha: 1
                }, 200, "Linear", true)

                var hide = game.add.tween(this.btnExitRedName).to({
                    alpha: 0
                }, 200, "Linear", true)
            }, this);

            this.btnContinue = game.add.sprite(120, 122, 'blueBtn');
            this.btnContinue.anchor.set(0.5);
            this.background.addChild(this.btnContinue);

            this.btnContinueName = game.add.text(0, 0, 'ПРОДОЛЖИТЬ');
            this.btnContinueName.anchor.set(0.5)
            this.btnContinueName.scale.set(0.4)
            this.btnContinueName.fontFamily = 'arial';
            this.btnContinueName.fontWeight = 'bold';
            this.btnContinueName.fill = "#ffd616";
            this.btnContinueName.fontSize = 36;
            this.btnContinue.addChild(this.btnContinueName);

            this.btnContinueRedName = game.add.text(0, 0, 'ПРОДОЛЖИТЬ');
            this.btnContinueRedName.anchor.set(0.5)
            this.btnContinueRedName.scale.set(0.4)
            this.btnContinueRedName.fontFamily = 'arial';
            this.btnContinueRedName.fontWeight = 'bold';
            this.btnContinueRedName.fill = "#ff101a";
            this.btnContinueRedName.fontSize = 36;
            this.btnContinueRedName.alpha = 0;
            this.btnContinue.addChild(this.btnContinueRedName);



            this.btnContinue.inputEnabled = true;
            this.btnContinue.events.onInputOver.add(function () {
                var hide = game.add.tween(this.btnContinueName).to({
                    alpha: 0
                }, 200, "Linear", true)

                var show = game.add.tween(this.btnContinueRedName).to({
                    alpha: 1
                }, 200, "Linear", true)

            }, this);

            this.btnContinue.events.onInputOut.add(function () {
                var show = game.add.tween(this.btnContinueName).to({
                    alpha: 1
                }, 200, "Linear", true)

                var hide = game.add.tween(this.btnContinueRedName).to({
                    alpha: 0
                }, 200, "Linear", true)
            }, this);
            this.background.addChild(this.btnContinue)
            this.background.visible = false
        },
        usersL: [],
        iterL: -11,
        addUser: function (place, user, coin, total) {
            this.line_graph = game.add.graphics(0, 0);
            this.line_graph.beginFill(0x165B7B, 1);
            this.line_graph.drawRoundedRect(0, 0, 470, 0.3);

            if (this.iterL == -11) {
                this.colorText = 'yellow';
            } else {
                this.colorText = 'white'
            }

            this.line_graphV = game.add.graphics(0, 0);
            this.line_graphV.beginFill(0x165B7B, 1);
            this.line_graphV.drawRoundedRect(0, 0, 0.3, 206);
            this.line_graphV.endFill();

            // for (var i = 0; i == 6; i++) {
            //     this.['lineH' + i] = game.add.sprite(0, -80, this.line_graph.generateTexture());
            //     this.['lineH' + i].anchor.set(0.5)
            //     this.tableGame.addChild(this.['lineH' + i]);
            // }

            this.lineH1 = game.add.sprite(0, -80, this.line_graph.generateTexture());
            this.lineH1.anchor.set(0.5)
            this.tableGame.addChild(this.lineH1);

            this.lineH2 = game.add.sprite(0, -54, this.line_graph.generateTexture());
            this.lineH2.anchor.set(0.5)
            this.tableGame.addChild(this.lineH2);

            this.lineH3 = game.add.sprite(0, -24, this.line_graph.generateTexture());
            this.lineH3.anchor.set(0.5)
            this.tableGame.addChild(this.lineH3);

            this.lineH4 = game.add.sprite(0, 4, this.line_graph.generateTexture());
            this.lineH4.anchor.set(0.5)
            this.tableGame.addChild(this.lineH4);

            this.lineH5 = game.add.sprite(0, 32, this.line_graph.generateTexture());
            this.lineH5.anchor.set(0.5)
            this.tableGame.addChild(this.lineH5);

            this.lineH6 = game.add.sprite(0, 60, this.line_graph.generateTexture());
            this.lineH6.anchor.set(0.5)
            this.tableGame.addChild(this.lineH6);

            this.lineV1 = game.add.sprite(-137, 0, this.line_graphV.generateTexture());
            this.lineV1.anchor.set(0.5)
            this.tableGame.addChild(this.lineV1)

            this.lineV2 = game.add.sprite(-3, 0, this.line_graphV.generateTexture());
            this.lineV2.anchor.set(0.5)
            this.tableGame.addChild(this.lineV2)

            this.lineV3 = game.add.sprite(115, 0, this.line_graphV.generateTexture());
            this.lineV3.anchor.set(0.5)
            this.tableGame.addChild(this.lineV3)

            this.textPl = game.add.text(-180, this.iterL, place);
            this.textPl.scale.set(0.2);
            this.textPl.fontFamily = 'arial';
            this.textPl.fontWeight = 'normal';
            this.textPl.fill = this.colorText;
            this.textPl.fontSize = 77;
            this.textPl.anchor.set(0.5);
            this.lineH1.addChild(this.textPl);

            this.textUs = game.add.text(-75, this.iterL, user);
            this.textUs.scale.set(0.2);
            this.textUs.fontFamily = 'arial';
            this.textUs.fontWeight = 'normal';
            this.textUs.fill = this.colorText;
            this.textUs.fontSize = 77;
            this.textUs.anchor.set(0.5);
            this.lineH1.addChild(this.textUs);

            this.textCt = game.add.text(60, this.iterL, coin);
            this.textCt.scale.set(0.2);
            this.textCt.fontFamily = 'arial';
            this.textCt.fontWeight = 'normal';
            this.textCt.fill = this.colorText;
            this.textCt.fontSize = 77;
            this.textCt.anchor.set(0.5);
            this.lineH1.addChild(this.textCt);

            this.textR = game.add.text(170, this.iterL, total);
            this.textR.scale.set(0.2);
            this.textR.fontFamily = 'arial';
            this.textR.fontWeight = 'normal';
            this.textR.fill = this.colorText;
            this.textR.fontSize = 77;
            this.textR.anchor.set(0.5);
            this.lineH1.addChild(this.textR);

            this.usersL.push(this.lineH1);
            this.iterL += 28;
        },
    }

    FINISH.createFinish()
    return FINISH
})

var DurakViewTable = (function() {
    var manager = {
        bars: [],
        cardsGroup: [],
        cards: {},
		tableCards: [],
        isDragableCard: true,
		seatsPosition: [{x: -10, y: -240},{x: 370, y: 50},{x: -370, y: -100}, {x: -370, y: 50}, {x: 370, y: -100}],
        init: function() {
            this.table = game.add.image(game.world.centerX, game.world.centerY, 'stol')
            this.table.scale.set(0.80)
            this.table.anchor.setTo(0.5)

			this.tableCardGroup = game.add.group()
            this.tableCardGroupDefeated = game.add.group()
            this.playerCardGroup = game.add.group()
			this.table.addChild(this.tableCardGroup)
            this.table.addChild(this.tableCardGroupDefeated)
            this.table.addChild(this.playerCardGroup)
            this.playerCardGroup.scale.set(0.8)
            this.playerCardGroup.y = 100
            this.tableCardGroupDefeated.scale.set(0.65)
            this.tableCardGroup.scale.set(0.65)
        },
        conectionLost: function() {
            var conLost = game.add.text(15, 15, 'Connection lost!')
            conLost.fill = "#ff0000"
            conLost.fontSize = 30
        },
        updateSeats: function() {
            for (let uId in this.bars)
                this.bars[uId].destroy()

            for (let uId in this.cardsGroup)
                this.cardsGroup[uId].destroy()
            this.cardsGroup = []
            this.bars = []

            this.busyPlace = 0
            for (let i = 0; i < table.players.length; i++)
                this.updateSeat(i);
        },
        startTimerWaitMove: function()
        {
            var self = this
            this.stopTimerWaitMove();
            this.timerBetActionSec = table.gameSpeed;

            this.timerWaitMove = setInterval(function ()
            {
                self.setProgress((self.timerBetActionSec / 60))
                self.timerBetActionSec--;

                if (self.timerBetActionSec < 0) self.stopTimerWaitMove();
            }, 1000);
        },
        win: function() {
            game.world.bringToTop(game.darkness)
            this.lastText = game.add.text(game.world.centerX, game.world.centerY, 'WIN')
            this.lastText.anchor.set(0.5)
            this.lastText.fill = "#ffffff"
            this.lastText.fontSize = 70
        },
        lose: function() {
            game.world.bringToTop(game.darkness)
            this.lastText = game.add.text(game.world.centerX, game.world.centerY, 'LOSE')
            this.lastText.anchor.set(0.5)
            this.lastText.fill = "#ffffff"
            this.lastText.fontSize = 70
        },
        stopTimerWaitMove: function()
        {
            clearInterval(this.timerWaitMove);
            this.timerWaitMove = 0;

            this.setProgress(1)
            // for (var i = 0; i < table.players.length; i++)
            // {
            //     document.getElementById("timer" + i).innerHTML = "";
            // }
        },
        createPlayerBar: function(login) {
            if (this.player_background)
                this.player_background.destroy()

            this.player_background = game.add.image(0, 275, 'atlas2', 'buttons')
            this.player_background.anchor.setTo(0.5)

            this.player_get = game.add.sprite(50, 0, 'atlas2', 'beru-norm')
            this.player_get.anchor.setTo(0.5)
            this.player_background.addChild(this.player_get)
            this.player_ready = game.add.sprite(150, 0, 'atlas2', 'bito-norm')
            this.player_ready.anchor.setTo(0.5)
            this.player_background.addChild(this.player_ready)
			this.player_get.inputEnabled = true

            var userCircle = utils.getSpriteFromG({x: -204, y: 0, radius: 80}, {c: 0xffffff}, {w: 6, c: 0x061c28}, 'circle')
            userCircle.anchor.set(0.5)

            var inactiveCircle = utils.getSpriteFromG({x: userCircle.x, y: userCircle.y, radius: 92}, {c: 0x1d4a60}, void 0, 'circle')
            inactiveCircle.anchor.set(0.5)

            var activeCircleBmp = game.add.bitmapData(200, 200);
            var grd = activeCircleBmp.context.createRadialGradient(100, 100, 0, 100, 100, 60);
            grd.addColorStop(0, '#ffd813');
            grd.addColorStop(0.83, '#ffd813');
            grd.addColorStop(0.86, '#ffd81367');
            grd.addColorStop(0.92, '#ffd81337');
            grd.addColorStop(1, '#ffd81300');

            activeCircleBmp.circle(100, 100, 60, grd);
            this.activeUserCircle = game.add.image(userCircle.x, userCircle.y, activeCircleBmp)
            this.activeUserCircle.anchor.set(0.5)
            this.activeUserCircle.alpha = 0

            this.player_background.addChild(inactiveCircle)
            this.player_background.addChild(this.activeUserCircle)
            this.player_background.addChild(userCircle)

	
			this.player_get.events.onInputOver.add(function() {
				this.player_get.frameName = 'beru-hov'
			}, this)
			this.player_get.events.onInputOut.add(function() {
				this.player_get.frameName = 'beru-norm'
			}, this)
			
			this.player_get.events.onInputDown.add(function() {
				sendGiveUp()
			}, this)
				
			this.player_ready.inputEnabled = true
			this.player_ready.events.onInputOver.add(function() {
				this.player_ready.frameName = 'bito-hov'
			}, this)
			
			this.player_ready.events.onInputOut.add(function() {
				this.player_ready.frameName = 'bito-norm'
			}, this)
			
			this.player_ready.events.onInputDown.add(function() {
				sendAttackEnd()
			}, this)

            this.atackEndActivate = function(is) {
                this.player_ready.inputEnabled = is
                this.player_ready.frameName = is ? 'bito-norm' : 'bito-dis'
            }

            this.giveUpActivate = function(is) {
                this.player_get.inputEnabled = is
                this.player_get.frameName = is ? 'beru-norm' : 'beru-dis'
            }

            this.player_name = game.add.text(-85, -8, login)
            this.player_name.fill = '#ffffff'
            this.player_name.fontSize = 19
            this.player_name.align = 'center'
            this.player_name.anchor.setTo(0.5)
            this.player_background.addChild(this.player_name)

            this.player_r = game.add.text(-85, 14, 'Рейтинг: 1280')
            this.player_r.fill = '#ffffff'
            this.player_r.fontSize = 14
            this.player_r.align = 'center'
            this.player_r.anchor.setTo(0.5)
            this.player_r.addColor('#ffda2e', 'Рейтинг: '.length);
            this.player_background.addChild(this.player_r)

            this.player_avatar = this.getAvatar(account.id)
            this.player_avatar.scale.set(0.28)
            this.player_avatar.anchor.set(0.5)
            userCircle.addChild(this.player_avatar)
			
			var oheight = 43
			this.progress_g = game.add.graphics(0, 0)
			this.progress_g.beginFill(0x45f146)
			this.progress_g.drawRoundedRect(0, 0, 19, oheight, 5)
			this.progress = game.add.sprite(220, 22, this.progress_g.generateTexture())
			this.progress.anchor.set(0.5, 1)
			this.progress_g.clear()
			this.progress_g.beginFill(0xf54146)
			this.progress_g.drawRoundedRect(0, 0, 19, oheight, 5)
			this.progress_red = game.add.sprite(0, 0, this.progress_g.generateTexture())
			this.progress_red.alpha = 0
			this.progress_red.anchor.set(0.5, 1)
			this.progress.addChild(this.progress_red)
			this.player_background.addChild(this.progress)

            game.add.tween(this.progress_red.scale).to({
                y: 1.7
            }, 300, "Linear", true, 700, -1, 700)

			//this.progress.height = 0
			this.progress_g.destroy()
			
			this.setProgress = function(val) {
				if (val > 1)
					val = 1
                if (val < 0)
                    val = 0
				var newHeight =  (val * oheight)
				manager.progress_red.alpha = 1 - val
				manager.progress.height = newHeight
				//game.add.tween(manager.progress, {height: newHeight}, 200, Phaser.Tween.Linear.None)
			}
            //this.player_background.scale.set(1.05)
            this.table.addChild(this.player_background)
        },
        activateUserCircle: function(uid) {
            var br = false
            this.activeUserCircle.alpha = 0
            for (var i in this.bars) {
                if (i == uid) {
                    this.bars[i].activate(true)
                    br = true
                }
                else
                    this.bars[i].activate(false)
            }

            if (!br) {
                game.add.tween(this.activeUserCircle).to( { alpha: 1 }, 100, Phaser.Easing.Linear.None, true);
            }
            
            //this.activeUserCircle.alpha = 0
            // if (this.bars[uid])
            //     this.bars[uid].activate(true)
            // else {
            //     this.activeUserCircle.alpha = 1
            //     game.add.tween(this.activeUserCircle).to( { alpha: 0 }, 500, Phaser.Easing.Linear.None, true);
            
            // }

        },
        createBar: function(x, y, uId, login) {
            var player_background = game.add.image(x, y, 'bg_account')
            player_background.anchor.setTo(0.5)

            var userCircle = utils.getSpriteFromG({x: (x > 0 ? 128 : -128), y: 0, radius: 95}, {c: 0xffffff}, {w: 7, c: 0x061c28}, 'circle')
            userCircle.anchor.set(0.5)

            var inactiveCircle = utils.getSpriteFromG({x: userCircle.x, y: userCircle.y, radius: 110}, {c: 0x1d4a60}, void 0, 'circle')
            inactiveCircle.anchor.set(0.5)

            var activeCircleBmp = game.add.bitmapData(200, 200);
            var grd = activeCircleBmp.context.createRadialGradient(100, 100, 0, 100, 100, 70);
            grd.addColorStop(0, '#ffd813');
            grd.addColorStop(0.78, '#ffd813');
            grd.addColorStop(0.781, '#ffd81367');
            grd.addColorStop(1, '#ffd81300');

            activeCircleBmp.circle(100, 100, 70, grd);
            var activeCircle = game.add.image(userCircle.x, userCircle.y, activeCircleBmp)
            activeCircle.anchor.set(0.5)
            activeCircle.alpha = uId == 34 ? 1 : 0

            player_background.addChild(inactiveCircle)
            player_background.addChild(activeCircle)
            player_background.addChild(userCircle)

            var player_name = game.add.text((x > 0 ? -34 : 34), -9, login)
            player_name.fill = '#ffffff'
            player_name.fontSize = 24
            player_name.align = 'center'
            player_name.anchor.setTo(0.5)
            player_background.addChild(player_name)

            var player_r = game.add.text(player_name.x, 19, 'Рейтинг: 1280')
            player_r.fill = '#ffffff'
            player_r.fontSize = 18
            player_r.align = 'center'
            player_r.anchor.setTo(0.5)
            player_r.addColor('#ffda2e', 'Рейтинг: '.length);
            player_background.addChild(player_r)

            player_avatar = this.getAvatar(uId)
            player_avatar.scale.set(0.325)
            player_avatar.anchor.set(0.5)
            userCircle.addChild(player_avatar)
            player_background.scale.set(0.75)

            player_background.activate = function(is) {
                game.add.tween(activeCircle).to( { alpha: (is ? 1 : 0) }, 100, Phaser.Easing.Linear.None, true);
            }
            return player_background

        },
        getAvatar: function(userId) {
            var avatar = game.add.image(0, 0, 'noavatar')

            if (game.cache.checkImageKey('avatar_' + userId)) {

            } else {
                game.loader.load('avatar_' + userId, 'https://steam-games.net/' + account.avatar, function() {

                })
            }
            return avatar
        },
		busyPlace: 0,
        updateSeat: function(seatIdx) {
            if (table.players[seatIdx].id) {
                if (table.players[seatIdx].id == account.id) {
                    this.createPlayerBar(table.players[seatIdx].login)
                } else {
                    var uId = table.players[seatIdx].id
                    this.cardsGroup[uId] = game.add.group()
                    this.table.addChild(this.cardsGroup[uId])
                    this.bars[uId] = this.createBar(this.seatsPosition[this.busyPlace].x, this.seatsPosition[this.busyPlace].y, uId, table.players[seatIdx].login)
                    this.cardsGroup[uId].scale.set(0.37)
                    this.cardsGroup[uId].y = this.bars[uId].y - 93
					this.busyPlace += 1
                    this.table.addChild(this.bars[uId])
                }
            } else {
                // delte bar
            }
        },
        createDeck: function() {
			this.trmpCard = this.getCard(table.trumpCard, false, false)
			this.trmpCard.x = -300
			this.trmpCard.y = -230
           // this.trmpCard.scale.set(0.5)
			this.table.addChild(this.trmpCard)

			this.deck = game.add.sprite(-30, 120, 'atlas', 'back')
			this.deck.scale.set(0.99)
			this.deck.angle = -90
			this.trmpCard.addChild(this.deck)

            var userCircle = utils.getSpriteFromG({x: 95, y: 57, radius: 95}, {c: 0xeeaa17}, {w: 3, c: 0x0e355c}, 'circle')
            userCircle.anchor.set(0.5)

            this.cardsCountBg = utils.getSpriteFromG({x: 0, y: 0, radius: 70}, {c: 0x1d4a60}, void 0, 'circle')
            this.cardsCountBg.anchor.set(0.5)
            userCircle.addChild(this.cardsCountBg)


			this.cardsCount = game.add.text(0, 0, '32')
			this.cardsCount.fill = '#eeaa17'
			this.cardsCount.fontSize = 36
            this.cardsCount.anchor.set(0.5, 0.45)
            this.cardsCountBg.addChild(this.cardsCount)
			this.trmpCard.addChild(userCircle)

            this.trmpCard.scale.set(0.6)


        },
        existRank: function(cards, rank) {
            for (let i = 0; i < cards.length; ++i)
                if (cards[i].rank == rank) return true;

            return false;
        },
        updateCardsDefending: function() {
            for (var i in this.cards[account.id]) {
                this.cards[account.id][i].destroy()
            }
            this.cards[account.id] = []

            for (let i = 0; i < table.playerCards.length; ++i)
            {
                let card = table.playerCards[i];
                this.cards[account.id][i] = this.getCard(card)
                this.playerCardGroup.add(this.cards[account.id][i])

                if ((card.suit != table.trumpCard.suit && card.suit == table.cards[table.cards.length - 1].suit && card.rank > table.cards[table.cards.length - 1].rank) || (card.suit == table.trumpCard.suit && ((table.trumpCard.suit != table.cards[table.cards.length - 1].suit) || (table.trumpCard.suit == table.cards[table.cards.length - 1].suit) && card.rank > table.cards[table.cards.length - 1].rank))) {

                    this.cards[account.id][i].inputEnabled = true

                    //if (this.isDragableCard) {
                        this.cards[account.id][i].input.enableDrag();

                        this.cards[account.id][i].events.onDragStart.add(function(sprite, pointer, x, y) {
                            sprite.angle = -8
                        }, this.cards[account.id][i]);

                        this.cards[account.id][i].events.onDragStop.add(function(sprite, pointer, x, y) {
                            sprite.angle = 0
                            var pos = sprite.game.input.getLocalPosition(sprite.parent, pointer);
                            if (pos.y < -10) {
                                sendDefending(card.rank, card.suit)
                            } else {
                                console.log(pos)
                                sprite.x = sprite.ox
                                sprite.y = 0
                            }
                        }, this.cards[account.id][i]);

                        this.cards[account.id][i].events.onDragUpdate.add(function (sprite, pointer, x, y) {
                            var pos = sprite.game.input.getLocalPosition(sprite.parent, pointer);
                            if (sprite.hitArea) {
                                sprite.x = pos.x - sprite.hitArea.width/2;
                                sprite.y = pos.y - sprite.hitArea.height/2;
                            } else {
                                sprite.x = pos.x - sprite.width / 2
                                sprite.y = pos.y - sprite.height / 2
                            }
                        }, this.cards[account.id][i]);
                    //} else {
                        this.cards[account.id][i].events.onInputDown.add(function(s, p) {
                            for (var idcv in manager.cards[account.id]) {
                                manager.cards[account.id][idcv].userEnabled = false
                                manager.cards[account.id][idcv].y = 0
                            }

                            if (p.msSinceLastClick < 300) {
                                sendDefending(card.rank, card.suit)
                            } else {
                                if (this.userEnabled) {
                                    this.userEnabled = false
                                   // this.y = 0
                                } else {
                                    this.userEnabled = true
                                    //this.y = -10
                                }
                            }
                        }, this.cards[account.id][i])
                   // }

                    this.cards[account.id][i].activate(true)
                }
            }
            this.playerCardGroup.align(320, -4, 83, 83);
            this.playerCardGroup.x = -this.playerCardGroup.width / 2
            for (var idcv in this.cards[account.id]) {
                this.cards[account.id][idcv].ox = this.cards[account.id][idcv].x
            }

            if (table.cards.length > 0) this.atackEndActivate(false);
            else this.atackEndActivate(false);
            this.giveUpActivate(true);

            if (table.state == GameState.WaitDefending)
                this.activateUserCircle(table.playerDefending)
        },
        updateCardsAttack: function() {
            for (var i in this.cards[account.id]) {
                this.cards[account.id][i].destroy()
            }
            this.cards[account.id] = []

            for (let i = 0; i < table.playerCards.length; ++i)
            {
                let card = table.playerCards[i];

                this.cards[account.id][i] = this.getCard(card)
                this.playerCardGroup.add(this.cards[account.id][i])
                if (table.cards.length == 0 || this.existRank(table.cards, card.rank)) {
                    this.cards[account.id][i].inputEnabled = true
                    //if (this.isDragableCard) {
                        this.cards[account.id][i].input.enableDrag();
                        this.cards[account.id][i].events.onDragStart.add(function(sprite, pointer, x, y) {
                            sprite.angle = -8
                        }, this.cards[account.id][i]);

                        this.cards[account.id][i].events.onDragStop.add(function(sprite, pointer, x, y) {
                            sprite.angle = 0
                            var pos = sprite.game.input.getLocalPosition(sprite.parent, pointer);
                            if (pos.y < -10) {
                                sendAttack(card.rank, card.suit)
                            } else {
                                console.log(pos)
                                sprite.x = sprite.ox
                                sprite.y = 0
                            }
                        }, this.cards[account.id][i]);

                        this.cards[account.id][i].events.onDragUpdate.add(function (sprite, pointer, x, y) {
                            var pos = sprite.game.input.getLocalPosition(sprite.parent, pointer);
                            if (sprite.hitArea) {
                                sprite.x = pos.x - sprite.hitArea.width/2;
                                sprite.y = pos.y - sprite.hitArea.height/2;
                            } else {
                                sprite.x = pos.x - sprite.width / 2
                                sprite.y = pos.y - sprite.height / 2
                            }
                        }, this.cards[account.id][i]);
                   // } else {
                        this.cards[account.id][i].events.onInputDown.add(function(s, p) {
                            for (var idcv in manager.cards[account.id]) {
                                manager.cards[account.id][idcv].userEnabled = false
                                manager.cards[account.id][idcv].y = 0
                            }

                            if (p.msSinceLastClick < 300) {
                                sendAttack(card.rank, card.suit)
                            } else {
                                if (this.userEnabled) {
                                    this.userEnabled = false
                                   // this.y = 0
                                } else {
                                    this.userEnabled = true
                                    //this.y = -10
                                }
                            }
                        }, this.cards[account.id][i])
                    //}

                    this.cards[account.id][i].activate(true)
                    
                }
            }
            this.playerCardGroup.align(320, -4, 83, 83);
            this.playerCardGroup.x = -this.playerCardGroup.width / 2
            for (var idcv in this.cards[account.id]) {
                this.cards[account.id][idcv].ox = this.cards[account.id][idcv].x
            }

            if (table.cards.length > 0) this.atackEndActivate(true);
            else this.atackEndActivate(false);
            this.giveUpActivate(false);
            if (table.state == GameState.WaitAttack)
                this.activateUserCircle(table.playerAttack)
        },
        cardToString: function(card, trmp) {
            var str = ''

            if (card.suit == CARD_SUIT.SPADES) str += "s";
            else if (card.suit == CARD_SUIT.CLUBS) str += "c";
            else if (card.suit == CARD_SUIT.DIAMONDS) str += "d";
            else if (card.suit == CARD_SUIT.HEARTS) str += "h";

            if (card.rank <= CARD_RANK._10) str += card.rank + 2;
            else if (card.rank == CARD_RANK.J) str += "j";
            else if (card.rank == CARD_RANK.Q) str += "q";
            else if (card.rank == CARD_RANK.K) str += "k";
            else if (card.rank == CARD_RANK.A) str += "a";

            return str
        },  
        getCard: function(card, ruba, isActivate) {
            var card
            if (ruba)
                card = game.add.image(0, 0, 'atlas', 'back')
            else
                card = game.add.image(0, 0, 'atlas', this.cardToString(card))
            card.activaE = utils.getSpriteFromG({x: 5, y: 5, width: card.width - 10, height: card.height - 10, radius: 5}, {c: 0xffd927, o: 0.45}, void 0, 'rounded')
            card.addChild(card.activaE)
            card.activaE.visible = isActivate
            card.activate = function(is) {
                card.activaE.visible = is
            }
            card.scale.set(0.9)
			card.userEnabled = false

            return card;
        },
        updateTableCards: function() {
			for (var i in this.tableCards) {
                this.tableCards[i].destroy()
            }
            this.tableCards = []

            for (let i = 0; i < table.cards.length; i += 2)
            {
                let card = table.cards[i];
                this.tableCards[i] = this.getCard(card, false, false)
                this.tableCardGroup.add(this.tableCards[i])
                if (table.cards[i + 1]) {
                    let card_dft = table.cards[i + 1];
                    if (!card_dft.defeat) {
                        this.tableCards[i+1] = this.getCard(card_dft, false, false)
                        this.tableCardGroupDefeated.add(this.tableCards[i+1])
                    } else {
                        this.tableCards[i+1] = this.getCard(card_dft, false, false)
                        this.tableCardGroup.add(this.tableCards[i+1])
                    }
                }
            }
            this.tableCardGroup.align(380, -4, 120, 120);
            this.tableCardGroupDefeated.align(380, -4, 120, 120);
            this.tableCardGroup.x = -this.tableCardGroup.width / 2
			this.tableCardGroup.y = -this.tableCardGroup.height * 0.7
            this.tableCardGroupDefeated.x = -this.tableCardGroup.width / 2
            this.tableCardGroupDefeated.y = -this.tableCardGroup.height * 0.7 + 60
        },
        updateCards: function() {
            let html = '';
            for (var i in this.cards[account.id]) {
                this.cards[account.id][i].destroy()
            }
            this.cards[account.id] = []

            for (let i = 0; i < table.playerCards.length; ++i)
            {
                let card = table.playerCards[i];
                this.cards[account.id][i] = this.getCard(card, false, false)
                this.playerCardGroup.add(this.cards[account.id][i])
            }
            this.playerCardGroup.align(320, -4, 83, 83);
            this.playerCardGroup.x = -this.playerCardGroup.width / 2

            if (table.state == GameState.WaitAttack)
                this.activateUserCircle(table.playerAttack)
            if (table.state == GameState.WaitDefending)
                this.activateUserCircle(table.playerDefending)

            this.atackEndActivate(false);
            this.giveUpActivate(false);


           // document.getElementById("cards" + table.accountIdx).innerHTML = html;

           // document.getElementById("btnAttackEnd").style.display = "none";
           // document.getElementById("btnGiveUp").style.display = "none";
        },
        updatePlayersCards: function() {
            for (let i = 0; i < table.players.length; ++i)
            {
                console.log(table.players[i])
                if (table.players[i].id != 0)
                {
                    var id = table.players[i].id
                    if (id != account.id)
                    {
                        for (var iss in this.cards[id]) {
                            this.cards[id][iss].destroy()
                        }
                        this.cards[id] = []
                        for (let iiss = 0; iiss < table.players[i].cardsCount; ++iiss) {
                            this.cards[id][iiss] = this.getCard(void 0, true, false)
                            this.cardsGroup[id].add(this.cards[id][iiss])
                        }
                        this.cardsGroup[id].align(220, -4, 40, 40);
                        this.cardsGroup[id].reverse()
                        this.cardsGroup[id].x = 0

                        if (this.bars[id].x <= 0)
                            this.cardsGroup[id].x = (this.bars[id].x + this.bars[id].width / 2) - this.cardsGroup[id].width
                        else
                            this.cardsGroup[id].x = (this.bars[id].x - this.bars[id].width / 2)
                        
                    }
                    else void 0;
                }
                else void 0;
            }
        }
    }

    manager.init()

	//manager.updateSeats()
	
	// setInterval(function() {
	// 	//manager.updateCards()
	// 	manager.updatePlayersCards()
	// }, 1000);
	
	// setInterval(function() {
	// 	manager.updateCardsAttack()
	// 	manager.updateTableCards()
	// }, 1500);
	
	// manager.createDeck()

  //  manager.createPlayerBar()
    return manager
})

var GameTablePopUp = (function () {
    "use strict";
    var x = game.world.centerX;
    var y = game.world.centerY;
    var width = 520;
    var height = 340;

    var TABLE = {
        users: [],
        background: undefined,
        topConteiner: {
            text: undefined
        },
        centerConteiner: {
            leftSide: {
                topC: {
                    text: undefined
                },
                down: undefined
            },
            rightSide: {
                topC: {
                    text: undefined
                },
                down: {
                    playBtn: undefined,
                    namePlay: undefined,
                    exitBtn: undefined,
                    nameExit: undefined
                }
            }
        },
        iter: 1,
        hide: function() {
            game.add.tween(this.background).to( { alpha: 0 }, 100, Phaser.Easing.Linear.None, true);
            game.add.tween(this.darkness).to( { alpha: 0 }, 100, Phaser.Easing.Linear.None, true);

            setTimeout(function() {
                this.background.visible = false
                this.darkness.visible = false
            }, 100)
            this.centerConteiner.rightSide.down.playBtn.inputEnabled = false
            this.centerConteiner.rightSide.down.exitBtn.inputEnabled = false
        },
        addGamer: function (id, user, online_user) {
            
            this.lineg = game.add.graphics(0, 0);

            this.lineg.beginFill(0xddf6f9);
            this.lineg.drawRoundedRect(0, 0, 244, 0.33)
            this.lineg.alpha = 0.3

            var line = game.add.sprite(10, this.iter * 33, this.lineg.generateTexture())
            line.username = id
            this.users.push(line);
            this.iter += 1
            this.lineg.destroy();
            this.centerConteiner.leftSide.down.addChild(line)

            //this.allGroup = game.add.group(this.userGroup)
            this.user_name = game.add.text(15, -25, user);
            this.user_name.fill = "#ddf6f9";
            this.user_name.fontSize = 20;
            this.user_name.fontFamily = 'arial';
            this.user_name.fontWeight = 500;
            this.user_name.scale.set(0.7);
            this.user_name.lineSpacing = 10;
            this.user_name.wordWrap = true;
            this.user_name.wordWrapWidth = this.centerConteiner.leftSide.topC.width * 0.865;
            line.addChild(this.user_name);

            this.button_ong = game.add.graphics(0, 0)
            this.button_ong.beginFill(0xFFD927, 1);
            this.button_ong.drawRoundedRect(0, 0, 120, 32, 5)
            this.button_ong.anchor.setTo(0.5)

            line.button_on = game.add.sprite(195, -15, this.button_ong.generateTexture())
            line.button_on.anchor.set(0.5)
            line.button_on.scale.setTo(0.7)
            this.button_ong.destroy()
            line.button_on.alpha = (online_user ? 1 : 0)
            line.addChild(line.button_on);

            this.button_text = game.add.text(0, 2, 'ИГРАЮ');
            this.button_text.anchor.setTo(0.5)
            this.button_text.fontSize = 18
            this.button_text.fill = '#7F5514'

            line.button_on.addChild(this.button_text)

            return this.user_name;
        },
        userReady: function (user) {
            for (var i in this.users) {
                if (this.users[i].username == user) {
                    this.users[i].button_on.alpha = 1
                    break
                }
            }
        },
        clearList: function() {
            this.iter = 1
            for (var i in this.users) {
                this.users[i].destroy()
            }
            this.users = []
        },
        createTable: function () {
            this.darkness = game.add.graphics(0,0)
            this.darkness.beginFill(0x000000)
            this.darkness.drawRect(0,0, game.width, game.height)
            this.darkness.alpha = 0.8

            this.backgroundg = game.add.graphics(0, 0)
            this.backgroundg.beginFill(0x0D374F, 1);
            this.backgroundg.drawRoundedRect(0, 0, width, height, 5)
            this.backgroundg.endFill();

            this.backgroundgt = game.add.graphics(0, 0)
            this.backgroundgt.beginFill(0x2190B8, 1);
            this.backgroundgt.drawRoundedRect(0, 0, width, 50, 5)
            this.backgroundgt.endFill();

            this.background = game.add.sprite(x, y, this.backgroundg.generateTexture());
            this.background.anchor.set(0.5)
            this.backgroundg.destroy()

            this.topBackground = game.add.sprite(-260, -170, this.backgroundgt.generateTexture())
            this.backgroundgt.destroy()
            this.background.addChild(this.topBackground)

            this.topConteiner = game.add.sprite(-5, -148, 'modal-shape_head');
            this.topConteiner.anchor.set(0.5);
            this.topConteiner.scale.set(1.09, 1.1);
            this.background.addChild(this.topConteiner)

            this.topConteiner.text = game.add.text(0, 2, "ИГРОВОЙ СТОЛ 19");
            this.topConteiner.text.anchor.setTo(0.5);
            this.topConteiner.text.scale.set(0.5, 0.45)
            this.topConteiner.text.fontFamily = 'arial';
            this.topConteiner.text.fontWeight = 700;
            this.topConteiner.text.fill = "#FFFFFF";
            this.topConteiner.text.fontSize = 40;
            this.topConteiner.text.wordWrap = true;
            this.topConteiner.text.wordWrapWidth = this.topConteiner.width;
            this.topConteiner.text.wordWrapHeight = this.topConteiner.height;
            this.topConteiner.addChild(this.topConteiner.text)

            this.centerConteiner.leftSide.topC = game.add.sprite(-106, -97, 'modal_shape');
            this.centerConteiner.leftSide.topC.scale.set(1.16, 1.1)
            this.centerConteiner.leftSide.topC.anchor.set(0.5)
            this.background.addChild(this.centerConteiner.leftSide.topC)

            this.centerConteiner.leftSide.down = game.add.sprite(-260, -77, 'modal-shape__user-list');
            this.centerConteiner.leftSide.down.scale.setTo(1.16, 1.16);
            this.background.addChild(this.centerConteiner.leftSide.down)

            this.centerConteiner.rightSide = game.add.graphics(200, -40);
            this.centerConteiner.rightSide.beginFill(225599, 1);
            this.centerConteiner.rightSide.drawRoundedRect(0, 0, width - this.centerConteiner.leftSide.width, height - this.topConteiner.height, 2);
            this.centerConteiner.rightSide.endFill();
            this.background.addChild(this.centerConteiner.rightSide)

            this.centerConteiner.rightSide.topC = game.add.sprite(156, -60, 'modal-shape__bets');
            this.centerConteiner.rightSide.topC.anchor.setTo(0.5);
            this.centerConteiner.rightSide.topC.scale.setTo(1, 1.15);
            this.background.addChild(this.centerConteiner.rightSide.topC)

            this.centerConteiner.rightSide.topC.text = game.add.text(0, -15, 'СТАВКА:');
            this.centerConteiner.rightSide.topC.text.anchor.setTo(0.5);
            this.centerConteiner.rightSide.topC.text.scale.setTo(0.5, 0.4);
            this.centerConteiner.rightSide.topC.text.fontFamily = 'arial';
            this.centerConteiner.rightSide.topC.text.fontWeight = 700;
            this.centerConteiner.rightSide.topC.text.fill = "white";
            this.centerConteiner.rightSide.topC.text.fontSize = 44;
            this.centerConteiner.rightSide.topC.text.wordWrap = true;
            this.centerConteiner.rightSide.topC.text.wordWrapWidth = this.centerConteiner.rightSide.topC.width;
            this.centerConteiner.rightSide.topC.addChild(this.centerConteiner.rightSide.topC.text)

            this.centerConteiner.rightSide.topC.text2 = game.add.text(0, -10, 'Игра на рейтинг');
            this.centerConteiner.rightSide.topC.text2.anchor.setTo(0.5, -0.5);
            this.centerConteiner.rightSide.topC.text2.scale.setTo(0.6, 0.45);
            this.centerConteiner.rightSide.topC.text2.fontFamily = 'arial';
            this.centerConteiner.rightSide.topC.text2.fontWeight = 700;
            this.centerConteiner.rightSide.topC.text2.fill = "#FFDA2E";
            this.centerConteiner.rightSide.topC.text2.fontSize = 33;
            this.centerConteiner.rightSide.topC.text2.wordWrap = true;
            this.centerConteiner.rightSide.topC.text2.wordWrapWidth = this.centerConteiner.rightSide.topC.width + 70;
            this.centerConteiner.rightSide.topC.addChild(this.centerConteiner.rightSide.topC.text2)

            this.centerConteiner.rightSide.down = game.add.sprite(156, 83, 'modal-shape__btns');
            this.centerConteiner.rightSide.down.anchor.setTo(0.5);
            this.centerConteiner.rightSide.down.scale.setTo(1, 1.15);
            this.background.addChild(this.centerConteiner.rightSide.down)

            this.centerConteiner.leftSide.topC.text = game.add.text(this.centerConteiner.leftSide.topC.x, this.centerConteiner.leftSide.topC.y, 'ГОТОВНОСТЬ ИГРОКОВ');
            this.centerConteiner.leftSide.topC.text.anchor.setTo(0.5);
            this.centerConteiner.leftSide.topC.text.scale.setTo(0.7);
            this.centerConteiner.leftSide.topC.text.fontFamily = 'arial';
            this.centerConteiner.leftSide.topC.text.fontWeight = 700;
            this.centerConteiner.leftSide.topC.text.fill = "white";
            this.centerConteiner.leftSide.topC.text.fontSize = 24;
            this.centerConteiner.leftSide.topC.text.wordWrap = true;
            this.centerConteiner.leftSide.topC.text.wordWrapWidth = this.centerConteiner.leftSide.topC.width;
            //this.grgame.add(this.centerConteiner.leftSide.topC.text)
            this.background.addChild(this.centerConteiner.leftSide.topC.text)

            this.btnEGrapgphics = game.add.graphics(0, 0);
            this.btnEGrapgphics.beginFill('0xC08913', 1)
            this.btnEGrapgphics.drawRoundedRect(-93, -65, 185, 64, 5)
            this.btnEGrapgphics.endFill()
            this.centerConteiner.rightSide.down.addChild(this.btnEGrapgphics)

            this.btnCGrapgphics = game.add.graphics(0, 0);
            this.btnCGrapgphics.beginFill('0x818181', 1)
            this.btnCGrapgphics.drawRoundedRect(-93, 0, 185, 64, 5)
            this.btnCGrapgphics.endFill()
            this.centerConteiner.rightSide.down.addChild(this.btnCGrapgphics)


            this.centerConteiner.rightSide.down.playBtn = game.add.sprite(0, -35, 'btn-yellow');
            this.centerConteiner.rightSide.down.playBtn.anchor.setTo(0.5);
            this.centerConteiner.rightSide.down.addChild(this.centerConteiner.rightSide.down.playBtn)

            this.btnOn = game.add.sprite(0, 0, 'glare_big');
            this.btnOn.scale.set(0.28, 0.3)
            this.btnOn.anchor.set(0.5, 0.5)
            this.centerConteiner.rightSide.down.playBtn.addChild(this.btnOn)

            this.btnOn2 = game.add.sprite(0, 0, 'glare');
            this.btnOn2.scale.set(0.2, 0.13)
            this.btnOn2.anchor.set(0.5, 0.5)
            this.centerConteiner.rightSide.down.playBtn.addChild(this.btnOn2)

            //btn on
            this.centerConteiner.rightSide.down.playBtn.inputEnabled = true;
            this.centerConteiner.rightSide.down.playBtn.events.onInputOver.add(function () {
                var animBtnOn = game.add.tween(this.btnEGrapgphics).to({
                    height: 62,
                    y: -2
                }, 100, "Linear", true)

                animBtnOn.onStart.add(function () {
                    this.centerConteiner.rightSide.down.namePlay.fill = 'red'
                }, this)
            }, this);

            this.centerConteiner.rightSide.down.playBtn.events.onInputOut.add(function () {
                var animBtnOn = game.add.tween(this.btnEGrapgphics).to({
                    height: 64,
                    y: 0
                }, 100, "Linear", true)

                animBtnOn.onStart.add(function () {
                    this.centerConteiner.rightSide.down.namePlay.fill = '#865C15'
                }, this)
            }, this)

            this.centerConteiner.rightSide.down.playBtn.events.onInputDown.add(function () {
                sendPlayConfirm()
            }, this)

            //exit btn
            this.centerConteiner.rightSide.down.exitBtn = game.add.sprite(0, 30, 'btn-white');
            this.centerConteiner.rightSide.down.exitBtn.anchor.setTo(0.5);
            this.centerConteiner.rightSide.down.addChild(this.centerConteiner.rightSide.down.exitBtn)

            this.centerConteiner.rightSide.down.exitBtn.inputEnabled = true;

            this.centerConteiner.rightSide.down.exitBtn.events.onInputOver.add(function () {
                var animBtnOff = game.add.tween(this.btnCGrapgphics).to({
                    height: 62,
                    y: 0,
                }, 100, "Linear", true);
                animBtnOff.onStart.add(function () {
                    this.centerConteiner.rightSide.down.nameExit.fill = 'red'
                    this.centerConteiner.rightSide.down.exitBtn.loadTexture('btn-yellow')
                }, this)
            }, this);

            this.centerConteiner.rightSide.down.exitBtn.events.onInputOut.add(function () {
                var animBtnOff = game.add.tween(this.btnCGrapgphics).to({
                    height: 64,
                    y: 1,
                }, 100, "Linear", true);
                animBtnOff.onStart.add(function () {
                    this.centerConteiner.rightSide.down.nameExit.fill = '#865C15'
                    this.centerConteiner.rightSide.down.exitBtn.loadTexture('btn-white')
                }, this)
            }, this);

            this.centerConteiner.rightSide.down.exitBtn.events.onInputDown.add(function () {
                alert()
            }, this);

            this.centerConteiner.rightSide.down.namePlay = game.add.text(this.centerConteiner.rightSide.down.playBtn.x, this.centerConteiner.rightSide.down.playBtn.y, 'Играю');
            this.centerConteiner.rightSide.down.namePlay.anchor.setTo(0.5);
            this.centerConteiner.rightSide.down.namePlay.scale.setTo(0.5, 0.45);
            this.centerConteiner.rightSide.down.namePlay.fontFamily = 'arial';
            this.centerConteiner.rightSide.down.namePlay.fontWeight = 700;
            // this.centerConteiner.rightSide.down.namePlay.addStrokeColor('#ddf6f9');
            // this.centerConteiner.rightSide.down.namePlay.strokeThickness = 1;
            this.centerConteiner.rightSide.down.namePlay.fill = "#865C15"
            this.centerConteiner.rightSide.down.namePlay.fontSize = 40;
            this.centerConteiner.rightSide.down.namePlay.wordWrap = true;
            this.centerConteiner.rightSide.down.namePlay.wordWrapWidth = this.centerConteiner.rightSide.down.width;
            this.centerConteiner.rightSide.down.addChild(this.centerConteiner.rightSide.down.namePlay)

            this.centerConteiner.rightSide.down.nameExit = game.add.text(this.centerConteiner.rightSide.down.exitBtn.x, this.centerConteiner.rightSide.down.exitBtn.y, 'Выйти');
            this.centerConteiner.rightSide.down.nameExit.anchor.setTo(0.5);
            this.centerConteiner.rightSide.down.nameExit.scale.setTo(0.5, 0.45);
            this.centerConteiner.rightSide.down.nameExit.fontFamily = 'arial';
            this.centerConteiner.rightSide.down.nameExit.fontWeight = 700;
            this.centerConteiner.rightSide.down.nameExit.fill = "#865C15";
            this.centerConteiner.rightSide.down.nameExit.fontSize = 40;
            this.centerConteiner.rightSide.down.nameExit.wordWrap = true;
            this.centerConteiner.rightSide.down.nameExit.wordWrapWidth = this.centerConteiner.rightSide.down.width;
            this.centerConteiner.rightSide.down.addChild(this.centerConteiner.rightSide.down.nameExit)
        }
    }

    TABLE.createTable();
    return TABLE;
});

var ChatManager = (function(argument) {
    "use strict";
    var manager = {
        vare: 34,
        init: function() {
           
        }
    }
    manager.init()
    return manager
})

var Boot = (function () {
    "use strict";
    var state = function() {

    };
    state.prototype = {
        init: function() {
            this.stage.backgroundColor = "#ffffff";
            game.input.maxPointers = 1;
            game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        },
        preload: function() {
            // body...
        },
        render: function() {
            this.state.start("Preloader")
        }
    }
    return state
})();

var Game = (function() {
    "use strict";
    var state = function() {

    };
    state.prototype = {
        init: function() {
           
        },
        preload: function() {
            // body...
        },

        create: function() {
            //game.chatManager = ChatManager();
            // game.worldManager = WorldManager();
            // game.FirstPopUpManager = FirstPopUpManager();
            //StateManager.startGame()
            game.stage.disableVisibilityChange = true;
            this.stage.backgroundColor = "#0D374F"
            game.isInputAllowed = false;

            this.Background_fon = game.add.tileSprite(0,0, game.width, game.height, 'background_mast')

            game.darkness = game.add.graphics(0,0)
            game.darkness.beginFill(0x000000)
            game.darkness.drawRoundedRect(0,0, game.width, game.height, -0.1)
            game.darkness.alpha = 0.0

            // this.table = game.add.image(game.world.centerX, game.world.centerY, 'stol')
            // game.Table.scale.set(0.75)
            // game.Table.anchor.setTo(0.5)

 
			game.loader = Loader()
            game.view = DurakViewTable()
            game.usrTable = GameTablePopUp()
            game.finishPopup = FinishPopUp()
           // game.finishPopup.showPopup()
            //finishPopUp

            client.Connect("www.steam-games.net", 13007, true);

        }
    }
    return state
})();

var Preloader = (function () {
    "use strict";
    var state = function() {

    };
    state.prototype = {

        init: function() {
            
        },
        onFileComplete: function(progress, cacheKey, success, totalLoaded, totalFiles) {
            this.loaderText.text = 'loading ' + progress + '%'
        },
        preload: function() {
            this.loaderText = this.add.text(game.world.centerX, game.world.centerY, "loading 0%");
            this.loaderText.fill = "#f1f1f1"

            this.load.onFileComplete.add(this.onFileComplete, this);

            game.load.atlas('atlas', '../durak_canvas/assets/cards_spritesheet.png', void 0, window.gameResources._spritesJson, 0)
            game.load.atlas('atlas2', '../durak_canvas/assets/spritesheet_buttons.png', void 0, window.gameResources._spritesJson2, 0)

            game.load.image('rubashka', '../durak_canvas/assets/back.png')
            game.load.image('background_mast', '../durak_canvas/assets/bg__field.png')
            game.load.image('stol', '../durak_canvas/assets/bg__table.png')
            game.load.image('playerbckg', '../durak_canvas/assets/table-shape.png')
            game.load.image('bg_account', '../durak_canvas/assets/bg--account.png')

            game.load.image('noavatar', '../durak_canvas/assets/noavatar.png')

            game.load.image('blueBtn', '../durak_canvas/assets/blueBtn.png')
            game.load.image('freeze', '../durak_canvas/assets/freeze.png')
            game.load.image('close', '../durak_canvas/assets/close.png')
            game.load.image('shapebtns', '../durak_canvas/assets/shapebtns.png')
            game.load.image('btn-yellow', '../durak_canvas/assets/btn-yellow.png')
            game.load.image('btn-white', '../durak_canvas/assets/btn-white.png')
            game.load.image('btn-msg', '../durak_canvas/assets/btn-msg.png')
            game.load.image('games_caption', '../durak_canvas/assets/games_caption.png')
            game.load.image('modal_shape', '../durak_canvas/assets/modal-shape__status.png')
            game.load.image('modal-shape_head', '../durak_canvas/assets/modal-shape_head.png')
            game.load.image('modal-shape__bets', '../durak_canvas/assets/modal-shape__bets.png')
            game.load.image('modal-shape__btns', '../durak_canvas/assets/modal-shape__btns.png')
            game.load.image('glare_big', '../durak_canvas/assets/glare_big.png')
            game.load.image('glare', '../durak_canvas/assets/glare.png')
            game.load.image('glare-form--left', '../durak_canvas/assets/glare-form--left.png')
            game.load.image('modal-shape__user-list', '../durak_canvas/assets/modal-shape__user-list.png')
            game.load.image('table-shapef', '../durak_canvas/assets/table-shapef.png')
        },
        create: function() {
            this.state.start("Game")
        },
        shutdown: function() {
            this.loaderText.destroy()
            // destroy bar
        }
    }
    return state
})();

var GameIndex = (function (width, height, parentEl) {
    "use strict";
    function init() {
        window.game = new Phaser.Game(width, height, Phaser.CANVAS, parentEl);
        game.mainState = game.state.add("Game", Game, false)
        game.state.add("Boot", Boot, false)
        game.state.add("Preloader", Preloader, false)
        game.state.start("Boot");

    }
    
    init();
    
});

setTimeout(function() {
   // alert('dddd')
    new GameIndex(1200, 600, 'game_content')
}, 0)


        // var PIXEL_RATIO = (function() {
        //     var ctx = document.createElement("canvas").getContext("2d"),
        //         dpr = window.devicePixelRatio || 1,
        //         bsr = ctx.webkitBackingStorePixelRatio ||
        //         ctx.mozBackingStorePixelRatio ||
        //         ctx.msBackingStorePixelRatio ||
        //         ctx.oBackingStorePixelRatio ||
        //         ctx.backingStorePixelRatio || 1;
        //         return dpr / bsr;
        // })();

        // var _GLOBAL = {
        //     Width: window.innerWidth,
        //     Height: window.innerHeight,
        //     Scale: 1
        // }

        // var game;

        // var UI_OBJ = {}
        // var PARAM = {}

        //     game = new Phaser.Game(, _GLOBAL.Height, Phaser.CANVAS, 'playable', { preload: preload, create: create, update: update });

        //     function preload() {

        //         game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        //         game.scale.pageAlignHorizontally = true;
        //         game.scale.pageAlignVertically = true
        //         game.stage.disableVisibilityChange = true;

        //         document.body.oncontextmenu = function() { return false; };
        //         Phaser.Canvas.setUserSelect(game.canvas, 'none');
        //         Phaser.Canvas.setTouchAction(game.canvas, 'none');
        //         game.stage.backgroundColor = '#000';

        //         game.load.image('rubashka', 'assets/back.png')
        //         game.load.image('background_mast', 'assets/bg__field.png')
        //         game.load.image('stol', 'assets/bg__table.png')

                              
        //     }

        //     function createUi() {

        //         var current_scaller = _GLOBAL.Width * 0.0005

        //         UI_OBJ.Background_stage = game.add.graphics(0,0);
        //         UI_OBJ.Background_stage.beginFill(0x0D374F) // #0D374F
        //         UI_OBJ.Background_stage.drawRoundedRect(0, 0, _GLOBAL.Width, _GLOBAL.Height, -0.1)

        //         UI_OBJ.Background_stage_fon = game.add.tileSprite(0,0,_GLOBAL.Width,_GLOBAL.Height, 'background_mast')
        //         UI_OBJ.Table = game.add.image(_GLOBAL.Width * 0.5,_GLOBAL.Height * 0.5, 'stol')
        //         //UI_OBJ.Table.scale.set(1)
        //         UI_OBJ.Table.anchor.setTo(0.5)


        //         drawCard()

        //     }

        //     function create() {

        //             createUi()

                     
        //             //drawCard(card)
        //     }


        //     function update() {


        //     }

        //     //var players = []
        //     var cardsOnTable = []

        //     // function pushplayer(playersinfo) {
        //     //     // playersinfo.id
        //     //     // playersinfo.username
        //     //     // playersinfo.time
        //     //     // playersinfo.decksize

        //     // }

        //     function drawCard(cardInfo) {
        //         // UI_OBJ.card_background = game.add.graphics(0,0)
        //         // UI_OBJ.same_card = game.add.image(0, 0, cardInfo.rank + '_' + cardInfo.suit)

        //         UI_OBJ.card = game.add.image(_GLOBAL.Width * 0.5, _GLOBAL.Height * 0.5, 'rubashka')
        //         UI_OBJ.card.scale.set(0.3)
        //         UI_OBJ.card.anchor.setTo(0.5)

        //         //UI_OBJ.sprite.card_id = cardsOnTable.length
        //         //ardsOnTable.push(sprite)
        //     }

        //     //event.card_id


        //     // function(event) {

        //     // }