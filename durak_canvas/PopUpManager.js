var FrozenPopUp = (function () {
        "use strict";

        var width = 470;
        var height = 210;
        var x = game.world.centerX;
        var y = game.world.centerY;
        var FROZEN = {
            background: undefined,
            topConteiner: {
                text: undefined,
                close: undefined
            },
            centerConteiner: {
                text: undefined
            },
            footerConteiner: {
                text: undefined
            },
            createFrozen: function () {
                this.groupGame = game.add.group()

                this.backgroundg = game.add.graphics(0, 0)
                this.backgroundg.beginFill('0x2079A4', 1)
                this.backgroundg.drawRoundedRect(0, 0, width, height, 5)
                this.backgroundg.endFill()

                this.background = game.add.sprite(x, y, this.backgroundg.generateTexture());
                this.background.anchor.set(0.5, 0.7);
                //this.groupGame.add(this.background);

                this.topConteiner = game.add.sprite(0, -145, 'blueBtn');
                this.topConteiner.anchor.set(0.5015, 0);
                this.topConteiner.scale.set(2.025, 1)
                this.background.addChild(this.topConteiner);

                this.centerConteiner = game.add.sprite(0, -45, 'freeze');
                this.centerConteiner.anchor.set(0.5);
                // this.groupGame.add(this.centerConteiner)
                this.background.addChild(this.centerConteiner);


                this.footerConteiner = game.add.sprite(0, 34, 'blueBtn');
                this.footerConteiner.anchor.set(0.5);
                this.footerConteiner.scale.set(2.02, 1.35)
                this.footerConteiner.inputEnabled = true;

                this.footerConteiner.events.onInputOver.add(function () {
                    this.footerConteiner.text.fill = 'red'
                }, this)
                this.footerConteiner.events.onInputOut.add(function () {
                    this.footerConteiner.text.fill = "#FFD331"
                }, this)

                this.footerConteiner.events.onInputDown.add(function () {
                    alert()
                }, this)

                //this.groupGame.add(this.footerConteiner)
                this.background.addChild(this.footerConteiner);

                this.topConteiner.close = game.add.sprite(95, 8, 'close');
                //this.topConteiner.close.anchor.set(0, 0);
                this.topConteiner.close.scale.setTo(0.15, 0.3)

                this.topConteiner.close.inputEnabled = true;
                this.topConteiner.close.events.onInputDown.add(function () {
                    alert()
                }, this)

                // this.groupGame.add(this.topConteiner.close)
                this.topConteiner.addChild(this.topConteiner.close);


                this.topConteiner.text = game.add.text(0, 0, "ВЫ ЗАМОРОЖЕНЫ");
                this.topConteiner.text.anchor.setTo(0.5, -0.25);
                this.topConteiner.text.scale.setTo(0.25, 0.5);
                this.topConteiner.text.font = 'serif';
                this.topConteiner.text.fill = "white";
                this.topConteiner.text.fontSize = 42;
                this.topConteiner.text.wordWrap = true;
                this.topConteiner.text.wordWrapWidth = this.topConteiner.width - (this.topConteiner.close.width * 2);
                this.topConteiner.text.wordWrapHeight = this.topConteiner.height;
                //this.groupGame.add(this.topConteiner.text)
                this.topConteiner.addChild(this.topConteiner.text)

                this.centerConteiner.text = game.add.text(0, -30, 'ВАШЕ ВРЕМЯ НА ХОД ЗАКОНЧИЛОСЬ!');
                this.centerConteiner.text.anchor.setTo(0.5);
                this.centerConteiner.text.scale.setTo(0.4);
                this.centerConteiner.text.font = 'cool';
                this.centerConteiner.text.addStrokeColor('#27433E');
                this.centerConteiner.text.strokeThickness = 15;
                this.centerConteiner.text.fill = "white";
                this.centerConteiner.text.fontSize = 42;
                this.centerConteiner.text.wordWrap = true;
                this.centerConteiner.text.wordWrapWidth = this.centerConteiner.width + 370;
                this.centerConteiner.text.wordWrapHeight = this.topConteiner.height + 50;
                //this.groupGame.add(this.centerConteiner.text)
                this.centerConteiner.addChild(this.centerConteiner.text)

                this.centerConteiner.text2 = game.add.text(0, 12, 'СЛЕДИТЕ ЗА ТАЙМЕРОМ ХОДА.  ');
                this.centerConteiner.text2.anchor.setTo(0.5, 1);
                this.centerConteiner.text2.scale.setTo(0.4);
                this.centerConteiner.text2.font = 'cool';
                this.centerConteiner.text2.addStrokeColor('#27433E');
                this.centerConteiner.text2.strokeThickness = 15;
                this.centerConteiner.text2.fill = "white";
                this.centerConteiner.text2.fontSize = 42;
                this.centerConteiner.text2.wordWrap = true;
                this.centerConteiner.text2.wordWrapWidth = this.centerConteiner.width + 370;
                this.centerConteiner.text2.wordWrapHeight = this.topConteiner.height + 50;
                // this.groupGame.add(this.centerConteiner.text)
                this.centerConteiner.addChild(this.centerConteiner.text2)

                this.centerConteiner.text3 = game.add.text(0, 40, 'НАЖМИТЕ "ПРОДОЛЖИТЬ" ДЛЯ ИГРЫ');
                this.centerConteiner.text3.anchor.setTo(0.5, 1);
                this.centerConteiner.text3.scale.setTo(0.4);
                this.centerConteiner.text3.font = 'cool';
                this.centerConteiner.text3.addStrokeColor('#27433E');
                this.centerConteiner.text3.strokeThickness = 15;
                this.centerConteiner.text3.fill = "white";
                this.centerConteiner.text3.fontSize = 42;
                this.centerConteiner.text3.wordWrap = true;
                this.centerConteiner.text3.wordWrapWidth = this.centerConteiner.width + 380;
                this.centerConteiner.text3.wordWrapHeight = this.topConteiner.height + 50;
                //this.groupGame.add(this.centerConteiner.text)
                this.centerConteiner.addChild(this.centerConteiner.text3)

                this.footerConteiner.text = game.add.text(0, 0, 'ПРОДОЛЖИТЬ');
                this.footerConteiner.text.anchor.setTo(0.5);
                this.footerConteiner.text.scale.setTo(0.2, 0.3);
                this.footerConteiner.text.font = 'cool';
                this.footerConteiner.text.fill = "#FFD331"
                this.footerConteiner.text.fontSize = 35;
                this.footerConteiner.text.wordWrap = true;
                this.footerConteiner.text.wordWrapWidth = this.footerConteiner.width - (this.topConteiner.close.width * 2);
                this.footerConteiner.text.wordWrapHeight = this.footerConteiner.height;
                this.backgroundg.destroy();
                //this.groupGame.add(this.footerConteiner.text)
                this.footerConteiner.addChild(this.footerConteiner.text)
            },
        };
        FROZEN.createFrozen()
        return FROZEN
    }
);

var GameTablePopUp = (function (data) {
        "use strict";
        var x = game.world.centerX;
        var y = game.world.centerY;
        var width = 520;
        var height = 340;

        var TABLE = {
            users: [],
            background: undefined,
            topConteiner: {
                text: undefined
            },
            centerConteiner: {
                leftSide: {
                    topC: {
                        text: undefined
                    },
                    down: undefined
                },
                rightSide: {
                    topC: {
                        text: undefined
                    },
                    down: {
                        playBtn: undefined,
                        namePlay: undefined,
                        exitBtn: undefined,
                        nameExit: undefined
                    }
                }
            },
            iter: 1,
            addGamer: function (user) {

                this.lineg = game.add.graphics(0, 0);

                this.lineg.beginFill(0xddf6f9);
                this.lineg.drawRoundedRect(0, 0, 244, 0.33)
                this.lineg.alpha = 0.3

                this.line = game.add.sprite(10, this.iter * 33, this.lineg.generateTexture())
                this.iter += 1
                this.lineg.destroy();
                this.centerConteiner.leftSide.down.addChild(this.line)


                //this.allGroup = game.add.group(this.userGroup)
                this.user_name = game.add.text(15, -25, user);
                this.user_name.fill = "#ddf6f9";
                this.user_name.fontSize = 20;
                this.user_name.fontFamily = 'arial';
                this.user_name.fontWeight = 500;
                this.user_name.scale.set(0.7);
                this.user_name.lineSpacing = 10;
                this.user_name.wordWrap = true;
                this.user_name.wordWrapWidth = this.centerConteiner.leftSide.topC.width * 0.865;
                this.line.addChild(this.user_name);

                this.button_ong = game.add.graphics(0, 0)
                this.button_ong.beginFill(0xFFD927, 1);
                this.button_ong.drawRoundedRect(0, 0, 120, 32, 5)
                this.button_ong.anchor.setTo(0.5)

                this.button_on = game.add.sprite(195, -15, this.button_ong.generateTexture())
                this.button_on.anchor.set(0.5)
                this.button_on.scale.setTo(0.7)
                this.button_on.alpha = 0
                this.button_ong.destroy()
                this.line.addChild(this.button_on);

                this.button_on.inputEnabled = true;
                this.button_on.events.onInputDown.add(function () {
                }, this)

                this.button_text = game.add.text(0, 2, 'ИГРАЮ');
                this.button_text.anchor.setTo(0.5)
                this.button_text.fontSize = 18;
                this.button_text.fill = '#7F5514'
                this.button_text.alpha = 0;

                this.button_on.addChild(this.button_text)
                this.users.push(this.line);
                //user_name.wordWrapHeight = this.centerConteiner.leftSide.height;
                //
                // if (online_user) {
                //
                // }

                return this.user_name;
            },
            playingCard: function (user) {
                for (var i in this.users) {
                    if (this.users[i].children[0].text == user) {
                        this.users[i].children[1].alpha = 1;
                        this.users[i].children[1].children[0].alpha = 1;
                    }
                }
            },
            createTable: function () {

                this.backgroundg = game.add.graphics(0, 0)
                this.backgroundg.beginFill(0x0D374F, 1);
                this.backgroundg.drawRoundedRect(0, 0, width, height, 5)
                this.backgroundg.endFill();

                this.backgroundgt = game.add.graphics(0, 0)
                this.backgroundgt.beginFill(0x2190B8, 1);
                this.backgroundgt.drawRoundedRect(0, 0, width, 50, 5)
                this.backgroundgt.endFill();

                this.background = game.add.sprite(x, y, this.backgroundg.generateTexture());
                this.background.anchor.set(0.5)
                this.backgroundg.destroy()

                this.topBackground = game.add.sprite(-260, -170, this.backgroundgt.generateTexture())
                this.backgroundgt.destroy()
                this.background.addChild(this.topBackground)

                this.topConteiner = game.add.sprite(-5, -148, 'modal-shape_head');
                this.topConteiner.anchor.set(0.5);
                this.topConteiner.scale.set(1.09, 1.1);
                this.background.addChild(this.topConteiner)

                this.topConteiner.text = game.add.text(0, 2, "ИГРОВОЙ СТОЛ 19");
                this.topConteiner.text.anchor.setTo(0.5);
                this.topConteiner.text.scale.set(0.5, 0.45)
                this.topConteiner.text.fontFamily = 'arial';
                this.topConteiner.text.fontWeight = 700;
                this.topConteiner.text.fill = "#FFFFFF";
                this.topConteiner.text.fontSize = 40;
                this.topConteiner.text.wordWrap = true;
                this.topConteiner.text.wordWrapWidth = this.topConteiner.width;
                this.topConteiner.text.wordWrapHeight = this.topConteiner.height;
                this.topConteiner.addChild(this.topConteiner.text)

                this.centerConteiner.leftSide.topC = game.add.sprite(-106, -97, 'modal_shape');
                this.centerConteiner.leftSide.topC.scale.set(1.16, 1.1)
                this.centerConteiner.leftSide.topC.anchor.set(0.5)
                this.background.addChild(this.centerConteiner.leftSide.topC)

                this.centerConteiner.leftSide.down = game.add.sprite(-260, -77, 'modal-shape__user-list');
                this.centerConteiner.leftSide.down.scale.setTo(1.16, 1.16);
                this.background.addChild(this.centerConteiner.leftSide.down)

                this.centerConteiner.rightSide = game.add.graphics(200, -40);
                this.centerConteiner.rightSide.beginFill(225599, 1);
                this.centerConteiner.rightSide.drawRoundedRect(0, 0, width - this.centerConteiner.leftSide.width, height - this.topConteiner.height, 2);
                this.centerConteiner.rightSide.endFill();
                this.background.addChild(this.centerConteiner.rightSide)

                this.centerConteiner.rightSide.topC = game.add.sprite(156, -60, 'modal-shape__bets');
                this.centerConteiner.rightSide.topC.anchor.setTo(0.5);
                this.centerConteiner.rightSide.topC.scale.setTo(1, 1.15);
                this.background.addChild(this.centerConteiner.rightSide.topC)

                this.centerConteiner.rightSide.topC.text = game.add.text(0, -15, 'СТАВКА:');
                this.centerConteiner.rightSide.topC.text.anchor.setTo(0.5);
                this.centerConteiner.rightSide.topC.text.scale.setTo(0.5, 0.4);
                this.centerConteiner.rightSide.topC.text.fontFamily = 'arial';
                this.centerConteiner.rightSide.topC.text.fontWeight = 700;
                this.centerConteiner.rightSide.topC.text.fill = "white";
                this.centerConteiner.rightSide.topC.text.fontSize = 44;
                this.centerConteiner.rightSide.topC.text.wordWrap = true;
                this.centerConteiner.rightSide.topC.text.wordWrapWidth = this.centerConteiner.rightSide.topC.width;
                this.centerConteiner.rightSide.topC.addChild(this.centerConteiner.rightSide.topC.text)

                this.centerConteiner.rightSide.topC.text2 = game.add.text(0, -10, 'Игра на рейтинг');
                this.centerConteiner.rightSide.topC.text2.anchor.setTo(0.5, -0.5);
                this.centerConteiner.rightSide.topC.text2.scale.setTo(0.6, 0.45);
                this.centerConteiner.rightSide.topC.text2.fontFamily = 'arial';
                this.centerConteiner.rightSide.topC.text2.fontWeight = 700;
                this.centerConteiner.rightSide.topC.text2.fill = "#FFDA2E";
                this.centerConteiner.rightSide.topC.text2.fontSize = 33;
                this.centerConteiner.rightSide.topC.text2.wordWrap = true;
                this.centerConteiner.rightSide.topC.text2.wordWrapWidth = this.centerConteiner.rightSide.topC.width + 70;
                this.centerConteiner.rightSide.topC.addChild(this.centerConteiner.rightSide.topC.text2)

                this.centerConteiner.rightSide.down = game.add.sprite(156, 83, 'modal-shape__btns');
                this.centerConteiner.rightSide.down.anchor.setTo(0.5);
                this.centerConteiner.rightSide.down.scale.setTo(1, 1.15);
                this.background.addChild(this.centerConteiner.rightSide.down)

                this.centerConteiner.leftSide.topC.text = game.add.text(this.centerConteiner.leftSide.topC.x, this.centerConteiner.leftSide.topC.y, 'ГОТОВНОСТЬ ИГРОКОВ');
                this.centerConteiner.leftSide.topC.text.anchor.setTo(0.5);
                this.centerConteiner.leftSide.topC.text.scale.setTo(0.7);
                this.centerConteiner.leftSide.topC.text.fontFamily = 'arial';
                this.centerConteiner.leftSide.topC.text.fontWeight = 700;
                this.centerConteiner.leftSide.topC.text.fill = "white";
                this.centerConteiner.leftSide.topC.text.fontSize = 24;
                this.centerConteiner.leftSide.topC.text.wordWrap = true;
                this.centerConteiner.leftSide.topC.text.wordWrapWidth = this.centerConteiner.leftSide.topC.width;
                //this.grgame.add(this.centerConteiner.leftSide.topC.text)
                this.background.addChild(this.centerConteiner.leftSide.topC.text)

                this.btnEGrapgphics = game.add.graphics(0, 0);
                this.btnEGrapgphics.beginFill('0xC08913', 1)
                this.btnEGrapgphics.drawRoundedRect(-93, -65, 185, 64, 5)
                this.btnEGrapgphics.endFill()
                this.centerConteiner.rightSide.down.addChild(this.btnEGrapgphics)

                this.btnCGrapgphics = game.add.graphics(0, 0);
                this.btnCGrapgphics.beginFill('0x818181', 1)
                this.btnCGrapgphics.drawRoundedRect(-93, 0, 185, 64, 5)
                this.btnCGrapgphics.endFill()
                this.centerConteiner.rightSide.down.addChild(this.btnCGrapgphics)


                this.centerConteiner.rightSide.down.playBtn = game.add.sprite(0, -35, 'btn-yellow');
                this.centerConteiner.rightSide.down.playBtn.anchor.setTo(0.5);
                this.centerConteiner.rightSide.down.addChild(this.centerConteiner.rightSide.down.playBtn)

                this.btnOn = game.add.sprite(0, 0, 'glare_big');
                this.btnOn.scale.set(0.28, 0.3)
                this.btnOn.anchor.set(0.5, 0.5)
                this.centerConteiner.rightSide.down.playBtn.addChild(this.btnOn)

                this.btnOn2 = game.add.sprite(0, 0, 'glare');
                this.btnOn2.scale.set(0.2, 0.13)
                this.btnOn2.anchor.set(0.5, 0.5)
                this.centerConteiner.rightSide.down.playBtn.addChild(this.btnOn2)

                //btn on
                this.centerConteiner.rightSide.down.playBtn.inputEnabled = true;
                this.centerConteiner.rightSide.down.playBtn.events.onInputOver.add(function () {
                    var animBtnOn = game.add.tween(this.btnEGrapgphics).to({
                        height: 62,
                        y: -2
                    }, 100, "Linear", true)

                    animBtnOn.onStart.add(function () {
                        this.centerConteiner.rightSide.down.namePlay.fill = 'red'
                    }, this)
                }, this);

                this.centerConteiner.rightSide.down.playBtn.events.onInputOut.add(function () {
                    var animBtnOn = game.add.tween(this.btnEGrapgphics).to({
                        height: 64,
                        y: 0
                    }, 100, "Linear", true)

                    animBtnOn.onStart.add(function () {
                        this.centerConteiner.rightSide.down.namePlay.fill = '#865C15'
                    }, this)
                }, this)

                this.centerConteiner.rightSide.down.playBtn.events.onInputDown.add(function () {
                    this.playingCard("usqwee");
                }, this)

                //exit btn
                this.centerConteiner.rightSide.down.exitBtn = game.add.sprite(0, 30, 'btn-white');
                this.centerConteiner.rightSide.down.exitBtn.anchor.setTo(0.5);
                this.centerConteiner.rightSide.down.addChild(this.centerConteiner.rightSide.down.exitBtn)

                this.centerConteiner.rightSide.down.exitBtn.inputEnabled = true;

                this.centerConteiner.rightSide.down.exitBtn.events.onInputOver.add(function () {
                    var animBtnOff = game.add.tween(this.btnCGrapgphics).to({
                        height: 62,
                        y: 0,
                    }, 100, "Linear", true);
                    animBtnOff.onStart.add(function () {
                        this.centerConteiner.rightSide.down.nameExit.fill = 'red'
                        this.centerConteiner.rightSide.down.exitBtn.loadTexture('btn-yellow')
                    }, this)
                }, this);

                this.centerConteiner.rightSide.down.exitBtn.events.onInputOut.add(function () {
                    var animBtnOff = game.add.tween(this.btnCGrapgphics).to({
                        height: 64,
                        y: 1,
                    }, 100, "Linear", true);
                    animBtnOff.onStart.add(function () {
                        this.centerConteiner.rightSide.down.nameExit.fill = '#865C15'
                        this.centerConteiner.rightSide.down.exitBtn.loadTexture('btn-white')
                    }, this)
                }, this);

                this.centerConteiner.rightSide.down.exitBtn.events.onInputDown.add(function () {
                    alert()
                }, this);

                this.centerConteiner.rightSide.down.namePlay = game.add.text(this.centerConteiner.rightSide.down.playBtn.x, this.centerConteiner.rightSide.down.playBtn.y, 'Играю');
                this.centerConteiner.rightSide.down.namePlay.anchor.setTo(0.5);
                this.centerConteiner.rightSide.down.namePlay.scale.setTo(0.5, 0.45);
                this.centerConteiner.rightSide.down.namePlay.fontFamily = 'arial';
                this.centerConteiner.rightSide.down.namePlay.fontWeight = 700;
                // this.centerConteiner.rightSide.down.namePlay.addStrokeColor('#ddf6f9');
                // this.centerConteiner.rightSide.down.namePlay.strokeThickness = 1;
                this.centerConteiner.rightSide.down.namePlay.fill = "#865C15"
                this.centerConteiner.rightSide.down.namePlay.fontSize = 40;
                this.centerConteiner.rightSide.down.namePlay.wordWrap = true;
                this.centerConteiner.rightSide.down.namePlay.wordWrapWidth = this.centerConteiner.rightSide.down.width;
                this.centerConteiner.rightSide.down.addChild(this.centerConteiner.rightSide.down.namePlay)

                this.centerConteiner.rightSide.down.nameExit = game.add.text(this.centerConteiner.rightSide.down.exitBtn.x, this.centerConteiner.rightSide.down.exitBtn.y, 'Выйти');
                this.centerConteiner.rightSide.down.nameExit.anchor.setTo(0.5);
                this.centerConteiner.rightSide.down.nameExit.scale.setTo(0.5, 0.45);
                this.centerConteiner.rightSide.down.nameExit.fontFamily = 'arial';
                this.centerConteiner.rightSide.down.nameExit.fontWeight = 700;
                this.centerConteiner.rightSide.down.nameExit.fill = "#865C15";
                this.centerConteiner.rightSide.down.nameExit.fontSize = 40;
                this.centerConteiner.rightSide.down.nameExit.wordWrap = true;
                this.centerConteiner.rightSide.down.nameExit.wordWrapWidth = this.centerConteiner.rightSide.down.width;
                this.centerConteiner.rightSide.down.addChild(this.centerConteiner.rightSide.down.nameExit)

                // this.userGroup = game.add.group()
                //this.userGroup.x = -180

                //this.background.addChild(this.userGroup)


                this.addGamer("usqwee")
                this.addGamer("uwws2222")
                this.addGamer("uwws22222")
                this.addGamer("uwws222222")
                this.addGamer("use")

            },

        }


        TABLE.createTable();
        return TABLE;
    }
);

var ChatPopUp = (function () {
    "use strict";
    var CHAT = {
        messages: [],
        fontSize: 13.001,
        addMessage: function (user, message) {
            this.messages.push({userId: user, text: message})
            this.messageGroup.add(this.getMessageItem(user, message))
        },
        getMessageItem: function (user, message) {
            var userText = game.add.text(0, this.messageGroup.height, user + ": " + message)
            userText.fill = "#ff0000"
            userText.fontSize = this.fontSize * this.bg_t.scale.x;
            userText.wordWrap = true
            userText.scale.set(1 / this.bg_t.scale.x)
            userText.wordWrapWidth = this.bg_b_white.width * this.bg_t.scale.x
            userText.addColor('#000000', user.length + 1);
            return userText
        },
        retranslateInput: function () {
            var bounds = {
                width: this.bg_b_white.width * this.bg_b.scale.x * this.bg_t.scale.x,
                height: this.bg_b_white.height * this.bg_b.scale.y * this.bg_t.scale.y
            }
            this.input.style.top = this.canvas.getBoundingClientRect().top + (this.bg_b_white.world.y - bounds.height / 2) - 4 + 'px';
            this.input.style.width = bounds.width + 'px';
            this.input.style.left = this.canvas.getBoundingClientRect().left + (this.bg_b_white.world.x - bounds.width / 2) + 'px';
            this.input.style.height = bounds.height + 'px';

            this.input.style.fontSize = (bounds.height * 0.70) + 'px'
        },

        createChat: function () {
            this.bg_t = game.add.sprite(game.world.centerX, game.world.centerY, 'shapebtns');
            this.bg_t.anchor.set(0.5)

            this.bg_b = game.add.sprite(0, 100, 'blueBtn');
            this.bg_b.anchor.set(0.5)
            this.bg_t.addChild(this.bg_b)
            this.bg_b.scale.set(0.88)

            this.bg_t_white_g = game.add.graphics(0, 0);
            this.bg_t_white_g.beginFill(0xffffff)
            this.bg_t_white_g.drawRoundedRect(0, 0, 190, 1034, 10)

            this.bg_t_white = game.add.sprite(0, -70, this.bg_t_white_g.generateTexture());
            this.bg_t_white_g.destroy()
            this.bg_t_white.anchor.set(0.5, 0)
            this.bg_t.addChild(this.bg_t_white)

            this.bg_b_white_g = game.add.graphics(0, 0);
            this.bg_b_white_g.beginFill(0xffffff)
            this.bg_b_white_g.drawRoundedRect(0, 0, 173, 30, 10)

            this.bg_b_white = game.add.sprite(-25, 0, this.bg_b_white_g.generateTexture());
            this.bg_b_white_g.destroy()
            this.bg_b_white.anchor.set(0.5)
            this.bg_b.addChild(this.bg_b_white)
            this.bg_t_white.inputEnabled = true
            this.bg_t_white.input.enableDrag(true);
            this.bg_t_white.input.allowHorizontalDrag = false
            this.bg_t_white.input.dragFromCenter = false;
            console.log(this.bg_b_white)

            this.bg_t_white.hitArea = new Phaser.Rectangle(-this.bg_t_white.width / 2, 0, this.bg_t_white.width, this.bg_t.height);

            this.bg_t_white.events.onDragUpdate.add(function (p) {
                this.bg_t_white.hitArea.y = -this.bg_t_white.y - this.bg_t.height / 2
                if (this.bg_t_white.y > -67)
                    this.bg_t_white.y = -67
                else if (this.bg_t_white.y < (-this.bg_t_white.height + 100))
                    this.bg_t_white.y = (-this.bg_t_white.height + 100)
            }, this)


            this.bg_t_white.boundsSprite = this.bg_t_white

            this.button = game.add.sprite(90, 0, 'btn-msg');
            this.button.anchor.set(0.5)
            this.button.scale.setTo(0.92)

            this.button.inputEnabled = true
            this.button.events.onInputDown.add(function () {
                if (this.input.value.length != 0) {
                    this.addMessage('User', this.input.value)
                    this.input.value = ''
                }
            }, this)

            this.input = document.createElement('input');
            this.canvas = document.querySelector('canvas');

            this.input.style.position = 'fixed';
            this.input.style.borderRadius = 5 + 'px';
            this.input.style.outline = 'none';
            this.input.style.fontSize = 20 + 'px';
            document.body.appendChild(this.input);

            setInterval(function () {
                CHAT.retranslateInput()
            }, 300)


            this.messageGroup = game.add.group()
            this.messageGroup.x = -90
            this.messageGroup.y = 10
            this.bg_t_white.addChild(this.messageGroup)

            this.m_mask = game.add.graphics(0, 0);

            this.m_mask.beginFill(0xffffff);

            this.m_mask.drawRoundedRect(-95, -67, 190, 134, 10);
            this.bg_t.addChild(this.m_mask)

            //	And apply it to the Sprite
            this.bg_t_white.mask = this.m_mask;

            this.bg_b.addChild(this.button)


            this.bg_t.scale.set(1)
            this.addMessage('fsdf', 'Hello mefrwe')
            this.addMessage('fsdf dfsf', 'He asdf sdfllo mefrwe')
            this.addMessage('fsdfsd f f', 'Helfsd fa sdflo mefrwe')
        },
    }
    CHAT.createChat()
    return CHAT;

})

var FinishPopUp = (function () {
    "use strict";
    var FINISH = {
        showPopup: function() {
            this.background.visible = this.dark.visible = this.btnExit.inputEnabled = this.btnContinue.inputEnabled = true;
        },
        createFinish: function () {
            this.dark = game.add.graphics(0,0);
            this.dark.beginFill(0x000000, 0.75);
            this.dark.drawRect(0,0, game.world.width, game.world.height);
            this.dark.inputEnabled = true
            this.dark.visible = false

            this.backgroundg = game.add.graphics(0, 0)
            this.backgroundg.beginFill(0x0D374F, 0.75)
            this.backgroundg.drawRoundedRect(0, 0, 477, 300, 5)
            this.backgroundg.endFill()

            this.backgroundTop = game.add.graphics(0, 0)
            this.backgroundTop.beginFill(0x2190B8, 0.75)
            this.backgroundTop.drawRoundedRect(0, 0, 477, 40, 5)
            this.backgroundTop.endFill()

            this.background = game.add.sprite(game.world.centerX, game.world.centerY, this.backgroundg.generateTexture());
            this.background.anchor.set(0.5);
            this.backgroundg.destroy();

            this.top = game.add.sprite(0, -129, this.backgroundTop.generateTexture())
            this.top.anchor.set(0.5);
            this.backgroundTop.destroy();
            this.background.addChild(this.top);

            this.topFrame = game.add.sprite(-4, -2, 'modal-shape_head');
            this.topFrame.anchor.set(0.5);
            this.top.addChild(this.topFrame);

            this.topName = game.add.text(0, 0, "ИТОГИ ИГРЫ");
            this.topName.anchor.set(0.4, 0.5);
            this.topName.scale.set(0.5)
            this.topName.fontFamily = 'arial';
            this.topName.fontWeight = 700;
            this.topName.fill = "#FFFFFF";
            this.topName.fontSize = 46
            this.top.addChild(this.topName);

            this.placeFrame = game.add.sprite(-232, -105, 'modal_shape');
            this.placeFrame.scale.set(0.35, 1)
            this.background.addChild(this.placeFrame);

            this.placeName = game.add.text(55, 6, 'МЕСТО');
            this.placeName.scale.set(0.5, 0.2);
            this.placeName.fontFamily = 'arial';
            this.placeName.fontWeight = 700;
            this.placeName.fill = "#FFFFFF";
            this.placeName.fontSize = 90;
            this.placeFrame.addChild(this.placeName);

            this.gamerFrame = game.add.sprite(-136, -105, 'modal_shape');
            this.gamerFrame.scale.set(0.5, 1)
            this.background.addChild(this.gamerFrame);

            this.gamerName = game.add.text(55, 6, 'ИГРОКИ');
            this.gamerName.scale.set(0.5, 0.25);
            this.gamerName.fontFamily = 'arial';
            this.gamerName.fontWeight = 700;
            this.gamerName.fill = "#FFFFFF";
            this.gamerName.fontSize = 75;
            this.gamerFrame.addChild(this.gamerName);

            this.coinFrame = game.add.sprite(0, -105, 'modal_shape');
            this.coinFrame.scale.set(0.43, 1)
            this.background.addChild(this.coinFrame);

            this.coinName = game.add.text(55, 6, 'PFP COIN');
            this.coinName.scale.set(0.5, 0.25);
            this.coinName.fontFamily = 'arial';
            this.coinName.fontWeight = 700;
            this.coinName.fill = "#FFFFFF";
            this.coinName.fontSize = 75;
            this.coinFrame.addChild(this.coinName);

            this.totalFrame = game.add.sprite(118, -105, 'modal_shape');
            this.totalFrame.scale.set(0.43, 1)
            this.background.addChild(this.totalFrame);

            this.totalName = game.add.text(55, 6, 'РЕЙТИНГ');
            this.totalName.scale.set(0.5, 0.25);
            this.totalName.fontFamily = 'arial';
            this.totalName.fontWeight = 700;
            this.totalName.fill = "#FFFFFF";
            this.totalName.fontSize = 75;
            this.totalFrame.addChild(this.totalName);

            this.tableGame = game.add.sprite(0, 35, 'table-shapef');
            this.tableGame.anchor.set(0.5)
            //this.tableGame.scale.set(2,2)
            this.background.addChild(this.tableGame);

            this.btnExit = game.add.sprite(-235, 100, 'blueBtn');
            this.background.addChild(this.btnExit);

            this.btnExitName = game.add.text(100, 12, 'ВЫЙТИ');
            this.btnExitName.scale.set(0.65, 0.8)
            this.btnExitName.fontFamily = 'arial';
            this.btnExitName.fontWeight = 800;
            this.btnExitName.fill = "#dfe2dd";
            this.btnExitName.fontSize = 16;
            this.btnExit.addChild(this.btnExitName);
            this.btnExit.inputEnabled = false;
            this.btnExit.events.onInputOver.add(function () {
                var a = game.add.tween(this.btnExitName).to({}, 100, "Linear", true)

                a.onStart.add(function () {
                    this.btnExitName.fill = '#dfe2dd'
                }, this)

                a.onComplete.add(function () {
                    this.btnExitName.fill = '#ad0600'
                }, this)
            }, this);

            this.btnExit.events.onInputOut.add(function () {
                var a = game.add.tween(this.btnExitName).to({}, 200, "Linear", true);

                a.onStart.add(function () {
                    this.btnExitName.fill = '#ad0600'
                }, this)

                a.onComplete.add(function () {
                    this.btnExitName.fill = '#dfe2dd'
                }, this)
            }, this);

            this.btnContinue = game.add.sprite(3, 100, 'blueBtn');
            this.background.addChild(this.btnContinue);

            this.btnContinueName = game.add.text(70, 12, 'ПРОДОЛЖИТЬ');
            this.btnContinueName.scale.set(0.65, 0.8)
            this.btnContinueName.fontFamily = 'arial';
            this.btnContinueName.fontWeight = 800;
            this.btnContinueName.fill = "#ffd616";
            this.btnContinueName.fontSize = 16;
            this.btnContinue.addChild(this.btnContinueName);
            this.btnContinue.inputEnabled = false;
            this.btnContinue.events.onInputOver.add(function () {
                var a = game.add.tween(this.btnContinueName).to({}, 100, "Linear", true)

                a.onStart.add(function () {
                    this.btnContinueName.fill = '#ffd616'
                }, this)

                a.onComplete.add(function () {
                    this.btnContinueName.fill = '#ff101a'
                }, this)
            }, this);

            this.btnContinue.events.onInputOut.add(function () {
                var a = game.add.tween(this.btnContinueName).to({}, 100, "Linear", true)

                a.onStart.add(function () {
                    this.btnContinueName.fill = '#ff101a'
                }, this)
                a.onComplete.add(function () {
                    this.btnContinueName.fill = '#ffd616'
                }, this)
            }, this);

            this.background.visible = false
        },
        usersL: [],
        iterL: -11,
        addUser: function (place, user, coin, total) {
            this.line_graph = game.add.graphics(0, 0);
            this.line_graph.beginFill(0x165B7B, 1);
            this.line_graph.drawRoundedRect(0, 0, 470, 0.3);

            if (this.iterL == -11) {
                this.colorText = 'yellow';
            } else {
                this.colorText = 'white'
            }

            this.line_graphV = game.add.graphics(0, 0);
            this.line_graphV.beginFill(0x165B7B, 1);
            this.line_graphV.drawRoundedRect(0, 0, 0.3, 206);
            this.line_graphV.endFill();

            // for (var i = 0; i == 6; i++) {
            //     this.['lineH' + i] = game.add.sprite(0, -80, this.line_graph.generateTexture());
            //     this.['lineH' + i].anchor.set(0.5)
            //     this.tableGame.addChild(this.['lineH' + i]);
            // }

            this.lineH1 = game.add.sprite(0, -80, this.line_graph.generateTexture());
            this.lineH1.anchor.set(0.5)
            this.tableGame.addChild(this.lineH1);

            this.lineH2 = game.add.sprite(0, -54, this.line_graph.generateTexture());
            this.lineH2.anchor.set(0.5)
            this.tableGame.addChild(this.lineH2);

            this.lineH3 = game.add.sprite(0, -24, this.line_graph.generateTexture());
            this.lineH3.anchor.set(0.5)
            this.tableGame.addChild(this.lineH3);

            this.lineH4 = game.add.sprite(0, 4, this.line_graph.generateTexture());
            this.lineH4.anchor.set(0.5)
            this.tableGame.addChild(this.lineH4);

            this.lineH5 = game.add.sprite(0, 32, this.line_graph.generateTexture());
            this.lineH5.anchor.set(0.5)
            this.tableGame.addChild(this.lineH5);

            this.lineH6 = game.add.sprite(0, 60, this.line_graph.generateTexture());
            this.lineH6.anchor.set(0.5)
            this.tableGame.addChild(this.lineH6);

            this.lineV1 = game.add.sprite(-137, 0, this.line_graphV.generateTexture());
            this.lineV1.anchor.set(0.5)
            this.tableGame.addChild(this.lineV1)

            this.lineV2 = game.add.sprite(-3, 0, this.line_graphV.generateTexture());
            this.lineV2.anchor.set(0.5)
            this.tableGame.addChild(this.lineV2)

            this.lineV3 = game.add.sprite(115, 0, this.line_graphV.generateTexture());
            this.lineV3.anchor.set(0.5)
            this.tableGame.addChild(this.lineV3)

            this.textPl = game.add.text(-180, this.iterL, place);
            this.textPl.scale.set(0.2, 0.21);
            this.textPl.fontFamily = 'arial';
            this.textPl.fontWeight = 550;
            this.textPl.fill = this.colorText;
            this.textPl.fontSize = 77;
            this.textPl.anchor.set(0.5);
            this.lineH1.addChild(this.textPl);

            this.textUs = game.add.text(-75, this.iterL, user);
            this.textUs.scale.set(0.2, 0.21);
            this.textUs.fontFamily = 'arial';
            this.textUs.fontWeight = 550;
            this.textUs.fill = this.colorText;
            this.textUs.fontSize = 77;
            this.textUs.anchor.set(0.5);
            this.lineH1.addChild(this.textUs);

            this.textCt = game.add.text(60, this.iterL, coin);
            this.textCt.scale.set(0.2, 0.21);
            this.textCt.fontFamily = 'arial';
            this.textCt.fontWeight = 550;
            this.textCt.fill = this.colorText;
            this.textCt.fontSize = 77;
            this.textCt.anchor.set(0.5);
            this.lineH1.addChild(this.textCt);

            this.textR = game.add.text(170, this.iterL, total);
            this.textR.scale.set(0.2, 0.21);
            this.textR.fontFamily = 'arial';
            this.textR.fontWeight = 550;
            this.textR.fill = this.colorText;
            this.textR.fontSize = 77;
            this.textR.anchor.set(0.5);
            this.lineH1.addChild(this.textR);

            this.usersL.push(this.lineH1);
            this.iterL += 28;
        },
    }

    FINISH.createFinish()
    return FINISH
})


