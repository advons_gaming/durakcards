var DurakTable = function ()
{
    this.id = 0;
    this.players = [{id: 1, login: 'Hello use name', cardsCount: 4}, {id: 34877, login: 'Frfsdfefdoer', cardsCount: 3}, {id: 34, login: 'Frefdoer', cardsCount: 6}, {id: 13, login: 'ffffsdfsdf', cardsCount: 12}, {id: 233, login: 'ffffsdfsdf', cardsCount: 4}, {id: 1343, login: 'Duglas Comp', cardsCount: 15}];
    this.accountIdx = -1;
    this.cards = [];

    this.deserialize = function (data)
    {
        this.id = data.GetUint32();
        let seatsCount = data.GetByte();
        this.deckSize = data.GetByte();
        this.gameSpeed = data.GetByte();

        this.players = [];
        for (let i = 0; i < seatsCount; ++i)
        {
            this.players[i] = {};
            this.players[i].id = data.GetUint32();
            this.players[i].login = data.GetString();
            this.players[i].avatar = data.GetString();
            this.players[i].cards = [];
        }
    }

    this.addPlayer = function (data)
    {
        const seatIdx = data.GetByte();
        this.players[seatIdx] = { id: data.GetUint32(), login: data.GetString(), avatar: data.GetString() }
        return seatIdx;
    }

    this.erasePlayer = function (data)
    {
        const accountId = data.GetUint32();
        const seatIdx = this.getPlayerIdx(accountId);
        if (seatIdx == -1) return seatIdx;
        this.clearSeat(seatIdx);
        return seatIdx;
    }

    this.startGame = function (data)
    {
        this.cards = [];
        this.accountIdx = -1;

        for (let i = 0; i < this.players.length; ++i)
        {
            this.players[i] = { id: data.GetUint32(), login: data.GetString(), avatar: data.GetString() }
            if (account.id != 0 && this.players[i].id == account.id) this.accountIdx = i;
        }

        this.trumpCard = { rank: data.GetByte(), suit: data.GetByte() };
        this.state = GameState.WaitAttack;
    }

    this.newRaund = function (data)
    {
        this.cards = [];

        this.playerAttack = data.GetUint32();
        this.playerDefending = data.GetUint32();
        this.deckCardsCount = data.GetByte();

        for (let i = 0; i < this.players.length; ++i)
        {
            this.players[i].cardsCount = data.GetByte();
        }

        this.state = GameState.WaitAttack;
    }

    this.setPlayerCards = function (data)
    {
        if (this.accountIdx == -1) return;

        this.playerCards = [];

        let cardsCount = data.GetByte();
        for (let j = 0; j < cardsCount; ++j)
            this.playerCards[j] = { rank: data.GetByte(), suit: data.GetByte() }

        this.playerCards.sort(this.sortCards);
    }

    this.attack = function (data)
    {
        let rank = data.GetByte();
        let suit = data.GetByte();

        this.cards[this.cards.length] = { rank, suit, defeat: true };

        if (this.playerAttack == account.id) this.eraseCard(rank, suit);
        else --this.players[this.getPlayerIdx(this.playerAttack)].cardsCount;

        this.state = data.GetByte();
        if (this.state == GameState.WaitAttack) this.playerAttack = data.GetUint32();
    }

    this.defending = function (data)
    {
        let rank = data.GetByte();
        let suit = data.GetByte();

        this.cards[this.cards.length] = { rank, suit };
        this.state = data.GetByte();
        if (this.state == GameState.WaitAttack) this.playerAttack = data.GetUint32();

        if (this.playerDefending == account.id) this.eraseCard(rank, suit);
        else --this.players[this.getPlayerIdx(this.playerDefending)].cardsCount;
    }

    this.attackEnd = function (data)
    {
        let playerAttackEnd = data.GetUint32();
        this.state = data.GetByte();
        if (this.state == GameState.WaitAttack) this.playerAttack = data.GetUint32();
    }

    this.giveUp = function (data)
    {
        this.state = data.GetByte();
        if (this.state == GameState.WaitAttack) this.playerAttack = data.GetUint32();
    }

    this.endGame = function (data)
    {
        this.cards = [];
        this.playerAttack = 0;
        this.playerDefending = 0;
        this.state = data.GetByte();
        let eraseCount = data.GetByte();
        
        for (let i = 0; i < eraseCount; ++i)
            this.erasePlayer(data);
    }

    this.technicalDefeat = function (data)
    {
        return this.erasePlayer(data);
    }

    this.sortCards = function (card1, card2)
    {
        if (card1.rank > card2.rank) return 1;
        if (card1.rank < card2.rank) return -1;
        return 0;
    }

    this.eraseCard = function (rank, suit)
    {
        for (let i = 0; i < this.playerCards.length; ++i)
        {
            if (this.playerCards[i].rank == rank && this.playerCards[i].suit == suit)
            {
                this.playerCards.splice(i, 1);
                break;
            }
        }
    }

    this.getPlayerIdx = function (playerId)
    {
        if (playerId == 0) return -1;

        for (let i = 0; i < this.players.length; ++i)
            if (playerId == this.players[i].id) return i;

        return -1;
    }

    this.clearSeat = function (seatIdx)
    {
        this.players[seatIdx] = { id:0, login: "", cardsCount: 0 }
    }
}
