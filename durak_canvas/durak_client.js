﻿var ClientCommand =
{
    Version: 0,
    Registration: 1,
    Auth: 2,

    BuyProduct: 3,
    FindAccount: 4,
    AddFriend: 5,

    CreateTable: 6,
    OpenTable: 7,
    CloseTable: 8,

    AddPlayer: 9,
    ErasePlayer: 10,

    WaitPlayersConfirm: 11,
    PlayerConfirm: 12,
    StartGame: 13,
    NewRaund: 14,
    SetPlayerCards: 15,
    Attack: 16,
    Defending: 17,
    AttackEnd: 18,
    GiveUp: 19,
    EndGame: 20,
    TechnicalDefeat: 21,
    Win: 22,
    Lose: 23,

    UpdateTableList: 24,
    UpdateBalance: 25,

    toString: function (com)
    {
        if (com == ClientCommand.Auth) return "Auth";

        else if (com == ClientCommand.CreateTable) return "CreateTable";
        else if (com == ClientCommand.OpenTable) return "OpenTable";
        else if (com == ClientCommand.CloseTable) return "CloseTable";

        else if (com == ClientCommand.AddPlayer) return "AddPlayer";
        else if (com == ClientCommand.ErasePlayer) return "ErasePlayer";

        else if (com == ClientCommand.WaitPlayersConfirm) return "WaitPlayersConfirm";
        else if (com == ClientCommand.PlayerConfirm) return "PlayerConfirm";
        else if (com == ClientCommand.StartGame) return "StartGame";
        else if (com == ClientCommand.NewRaund) return "NewRaund";
        else if (com == ClientCommand.SetPlayerCards) return "SetPlayerCards";
        else if (com == ClientCommand.Attack) return "Attack";
        else if (com == ClientCommand.Defending) return "Defending";
        else if (com == ClientCommand.AttackEnd) return "AttackEnd";
        else if (com == ClientCommand.GiveUp) return "GiveUp";
        else if (com == ClientCommand.EndGame) return "EndGame";
        else if (com == ClientCommand.TechnicalDefeat) return "TechnicalDefeat";
        else if (com == ClientCommand.Win) return "Win";
        else if (com == ClientCommand.Lose) return "Lose";

        else if (com == ClientCommand.UpdateTableList) return "UpdateTableList";
        else if (com == ClientCommand.UpdateBalance) return "UpdateBalance";
        return "";
    }
};

var Error =
{
    Success: 0,
    Version: 1,
    Parameters: 2,
    Concurrent: 3,
    SendMail: 4,
    BCrypt: 5,
    WrongLoginPass: 6,
    NotEnoughMoney: 7,

    NotFreeSeats: 8,
    ActionSequence: 9,
    QueryDatabase: 10,

    WrongCards: 11,
};

var GameState =
{
    WaitPlayers: 0,
    WaitPlayersConfirm: 1,
    Start: 2,
    NewRaund: 3,
    WaitAttack: 4,
    WaitDefending: 5,
    End: 6,
};

var CARD_SUIT =  // Масть
{
    SPADES: 0, // пики
    CLUBS: 1, // трефы
    DIAMONDS: 2, // бубны
    HEARTS: 3 // червы
};

var CARD_RANK =  // достоинство карты
{
    _2: 0,
    _3: 1,
    _4: 2,
    _5: 3,
    _6: 4,
    _7: 5,
    _8: 6,
    _9: 7,
    _10: 8,
    J: 9, // Jack - валет
    Q: 10, // Queen - дама
    K: 11, // King - король
    A: 12 // Ace - туз
};

var CARD_SUIT =  // Масть
{
    SPADES: 0, // пики
    CLUBS: 1, // трефы
    DIAMONDS: 2, // бубны
    HEARTS: 3 // червы
};

var account = {id: 1};
var tables = [];
var table = new DurakTable;
var client = new ClientWS();
client.onMessage = onMessage;

client.onOpen = function (evt)
{
    console.log('Сервер подключен');
    sendLogin();
};

client.onClose = function (evt)
{
    console.log('CLIENT CLOSE ' + evt.reason + evt.code);
    console.log(evt);
    game.view.conectionLost()
    onCloseConnection();
};

client.onError = function (evt)
{
    console.error('CLIENT ERROR');
    console.log(evt);
};

function onMessage(data)
{
    let error = Error.Success;
    let tableId = 0;
    let payload = {};
    let command = data.GetShort();
    console.log(ClientCommand.toString(command));
    switch (command)
    {
        case ClientCommand.Auth:
            error = data.GetByte();
            if (error == Error.Success)
            {
                account.id = data.GetUint32();
                account.login = data.GetString();
                account.avatar = data.GetString();
                if (account.avatar.length == 0) account.avatar = '/upload/avatars/noavatar.png';
                account.balance = [];
                account.balance[0] = data.GetInt32();
                account.balance[1] = data.GetInt32();
                account.balance[2] = data.GetInt32();
                console.log(account);

                tables = [];
                let tablesCount = data.GetShort();
                for (let i = 0; i < tablesCount; ++i)
                    tables[i] = { id: data.GetUint32(), seatsCount: data.GetByte(), deckSize: data.GetByte(), gameSpeed: data.GetByte(), playersCount: data.GetByte() }

                console.log(tables);

                 //   alert('ffff')
                 var tableNumber = window.localStorage.getItem('durak_tableId')
                 if (typeof game != 'undefined') {
                    if (tableNumber) {
                        window.localStorage.removeItem('durak_tableId')
                        sendOpenTable(tableNumber)
                    }
                 }
               // sendOpenTable(70)
                //onAuth();
            }
            break;

        case ClientCommand.CreateTable:
            error = data.GetByte();
            if (error == Error.Success)
            {
                table.deserialize(data);
                console.log(table);
            }
            break;

        case ClientCommand.OpenTable:
            error = data.GetByte();
            if (error == Error.Success)
            {
                table.deserialize(data);
                console.log(table);
            }
            break;

        case ClientCommand.AddPlayer:
            tableId = data.GetUint32();
            payload = table.addPlayer(data);
            console.log(table);
            break;

        case ClientCommand.ErasePlayer:
            tableId = data.GetUint32();
            payload = table.erasePlayer(data);
            console.log(table);
            break;

        case ClientCommand.PlayerConfirm:
            tableId = data.GetUint32();
            payload = data.GetUint32();
            break;

        case ClientCommand.StartGame:
            tableId = data.GetUint32();
            table.startGame(data);
            console.log(table);
            break;

        case ClientCommand.NewRaund:
            tableId = data.GetUint32();
            table.newRaund(data);
            console.log(table);
            break;

        case ClientCommand.SetPlayerCards:
            tableId = data.GetUint32();
            table.setPlayerCards(data);
            console.log(table);
            break;

        case ClientCommand.Attack:
            tableId = data.GetUint32();
            error = data.GetByte();
            if (error == Error.Success)
            {
                table.attack(data);
            }
            else console.error(error);
            console.log(table);
            break;

        case ClientCommand.Defending:
            tableId = data.GetUint32();
            error = data.GetByte();
            if (error == Error.Success)
            {
                table.defending(data);
            }
            else console.error(error);
            console.log(table);
            break;

        case ClientCommand.AttackEnd:
            tableId = data.GetUint32();
            table.attackEnd(data);
            console.log(table);
            break;

        case ClientCommand.GiveUp:
            tableId = data.GetUint32();
            table.giveUp(data);
            console.log(table);
            break;

        case ClientCommand.EndGame:
            tableId = data.GetUint32();
            table.endGame(data);
            console.log(table);
            break;

        case ClientCommand.TechnicalDefeat:
            tableId = data.GetUint32();
            table.technicalDefeat(data);
            console.log(table);
            break;

        case ClientCommand.Win:
            tableId = data.GetUint32();
            payload = { accountId: data.GetUint32(), winSumm: data.GetInt32() };
            break;

        case ClientCommand.Lose:
            tableId = data.GetUint32();
            payload = { accountId: data.GetUint32(), loseSumm: data.GetInt32() };
            break;

        case ClientCommand.UpdateTableList:
            var updateType = data.GetByte();
            console.log("updateType " + updateType);
            if (updateType == 0)
            {
                tables[tables.length] = { id: data.GetUint32(), seatsCount: data.GetByte(), deckSize: data.GetByte(), gameSpeed: data.GetByte(), playersCount: data.GetByte() }
            }
            else if (updateType == 1)
            {
                tableId = data.GetUint32();
                for (var i = 0; i < tables.length; i++)
                    if (tables[i].id == tableId)
                    {
                        tables.splice(i, 1);
                        break;
                    }
            }
            else if (updateType == 2)
            {
                tableId = data.GetUint32();

                for (var i = 0; i < tables.length; i++)
                    if (tables[i].id == tableId)
                    {
                        tables[i].playersCount = data.GetByte();
                        break;
                    }
            }

            console.log(tables);
            break;

        case ClientCommand.UpdateBalance:
            let currencyIdx = data.GetByte();
            account.balance[currencyIdx] = data.GetInt32();
            console.log(account.balance);
            break;
   }

    onReceive(command, error, payload);
}

function sendLogin()
{
    client.com.AddShort(ClientCommand.Auth);
    client.com.AddByte(4);
    //client.com.AddByte(1);
    //client.com.AddString("1aflc0nlaacdr5h2hnes7est1p");
    client.Send();
}

function sendCreateTable(seatsCount, deckSize, gameSpeed, bet)
{
    client.com.AddShort(ClientCommand.CreateTable);
    client.com.AddByte(seatsCount);
    client.com.AddByte(deckSize);
    client.com.AddByte(gameSpeed);
    client.com.AddInt32(bet);
    client.Send();
}

function sendOpenTable(id)
{
    client.com.AddShort(ClientCommand.OpenTable);
    client.com.AddUint32(id);
    client.Send();
}

function sendCloseTable(id)
{
    client.com.AddShort(ClientCommand.CloseTable);
    client.com.AddUint32(id);
    client.Send();
}

function sendPlayConfirm()
{
    client.com.AddShort(ClientCommand.PlayerConfirm);
    client.com.AddUint32(table.id);
    client.Send();
}

function sendAttack(rank, suit)
{
    client.com.AddShort(ClientCommand.Attack);
    client.com.AddUint32(table.id);
    client.com.AddByte(rank);
    client.com.AddByte(suit);
    client.Send();
}

function sendDefending(rank, suit)
{
    client.com.AddShort(ClientCommand.Defending);
    client.com.AddUint32(table.id);
    client.com.AddByte(rank);
    client.com.AddByte(suit);
    client.Send();
}

function sendAttackEnd()
{
    client.com.AddShort(ClientCommand.AttackEnd);
    client.com.AddUint32(table.id);
    client.Send();
}

function sendGiveUp()
{
    client.com.AddShort(ClientCommand.GiveUp);
    client.com.AddUint32(table.id);
    client.Send();
}

