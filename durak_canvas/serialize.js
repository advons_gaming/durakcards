"use strict";
var Serialize = function ()
{
    var encode = {output: [], outputSize: 0};
    var encodeVarint = function (value, encode) {
        encode.outputSize = 0;
        while (value > 127) {
            encode.output[encode.outputSize] = (value & 127) | 128;
            value >>= 7;
            encode.outputSize++;
        }
        encode.output[encode.outputSize++] = value & 127;
    };

    var decode = {input: [], size: 0};
    var decodeVarint = function (decode) {
        var ret = 0;
        decode.size = 0;
        for (var i = 0; i < 10; i++) {
            ret |= (decode.input[i] & 127) << (7 * i);
            decode.size++;
            if (!(decode.input[i] & 128)) {
                break;
            }
        }
        return ret;
    };

    var Public =
    {
        Buffer: new ArrayBuffer(0),
        BufferPnt: -1,
        BufferSize: 0,

        Reset: function () {
            this.BufferPnt = 0;
        },

        Clear: function () {
            delete this.Buffer;
            this.Buffer = new ArrayBuffer(0);
            this.BufferPnt = -1;
            this.BufferSize = 0;
        },

        AddByte: function (byte) {
            this.BufferSize++;
            this.BufferPnt++;
            var newBuffer = new ArrayBuffer(this.BufferSize);
            var byteBuffer = new Uint8Array(newBuffer);
            var oldBuffer = new Uint8Array(this.Buffer);
            for (var i = 0; i < this.BufferSize; i++) {
                byteBuffer[i] = oldBuffer[i];
            }
            byteBuffer[this.BufferPnt] = byte;
            delete this.Buffer;
            this.Buffer = newBuffer;
        },

        AddBool: function (bool) {
            bool ? AddByte (1) : AddByte (0);
        },

        AddShort: function (short) {
            var newBuffer = new ArrayBuffer(this.BufferSize + 2);
            var tempBuffer = new ArrayBuffer(2);
            var tempShortBuffer = new Uint16Array(tempBuffer);
            tempShortBuffer[0] = short;
            var tempByteBuffer = new Uint8Array(tempBuffer);
            var byteBuffer = new Uint8Array(newBuffer);
            var oldBuffer = new Uint8Array(this.Buffer);
            for (var i = 0; i < this.BufferSize; i++) {
                byteBuffer[i] = oldBuffer[i];
            }
            for (var j = this.BufferSize; j < this.BufferSize + 2; j++) {
                byteBuffer[j] = tempByteBuffer[j - this.BufferSize];
            }
            delete this.Buffer;
            this.Buffer = newBuffer;
            this.BufferPnt += 2;
            this.BufferSize += 2;
        },

        AddUint32: function (uint32) {
            var newBuffer = new ArrayBuffer(this.BufferSize + 4);
            var tempBuffer = new ArrayBuffer(4);
            var tempUint32Buffer = new Uint32Array(tempBuffer);
            tempUint32Buffer[0] = uint32;
            var tempByteBuffer = new Uint8Array(tempBuffer);
            var byteBuffer = new Uint8Array(newBuffer);
            var oldBuffer = new Uint8Array(this.Buffer);
            for (var i = 0; i < this.BufferSize; i++) {
                byteBuffer[i] = oldBuffer[i];
            }
            for (var j = this.BufferSize; j < this.BufferSize + 4; j++) {
                byteBuffer[j] = tempByteBuffer[j - this.BufferSize];
            }
            delete this.Buffer;
            this.Buffer = newBuffer;
            this.BufferPnt += 4;
            this.BufferSize += 4;
        },

		AddInt32: function (int32)
		{
			var newBuffer = new ArrayBuffer(this.BufferSize + 4);
			var tempBuffer = new ArrayBuffer(4);
			var tempUint32Buffer = new Int32Array(tempBuffer);
			tempUint32Buffer[0] = int32;
			var tempByteBuffer = new Uint8Array(tempBuffer);
			var byteBuffer = new Uint8Array(newBuffer);
			var oldBuffer = new Uint8Array(this.Buffer);
			for (var i = 0; i < this.BufferSize; i++)
			{
				byteBuffer[i] = oldBuffer[i];
			}
			for (var j = this.BufferSize; j < this.BufferSize + 4; j++)
			{
				byteBuffer[j] = tempByteBuffer[j - this.BufferSize];
			}
			delete this.Buffer;
			this.Buffer = newBuffer;
			this.BufferPnt += 4;
			this.BufferSize += 4;
		},


        AddUint64: function (uint64) {
            var newBuffer = new ArrayBuffer(this.BufferSize + 8);
            var byteBuffer = new Uint8Array(newBuffer);
            var tempBuffer = new ArrayBuffer(8);
            var tempByteBuffer = new Uint8Array(tempBuffer);
            var hex = uint64.toString(16);
            var hexlen = 16-hex.length;
            for(var h = 0; h < hexlen; h++)hex = "0" + hex;
            for(var a = 7; a>=0; a--)tempByteBuffer[a] = parseInt(hex.substr(14-(a*2),2), 16);
                 var oldBuffer = new Uint8Array(this.Buffer);
            for (var i = 0; i < this.BufferSize; i++)byteBuffer[i] = oldBuffer[i];
            for (var j = this.BufferSize; j < this.BufferSize + 8; j++)
            {
                byteBuffer[j] = tempByteBuffer[j - this.BufferSize];
            }
            delete this.Buffer;
            this.Buffer = newBuffer;
            this.BufferPnt += 8;
            this.BufferSize += 8;
        },

        AddDouble: function (double) {
            var newBuffer = new ArrayBuffer(this.BufferSize + 8);
            var tempBuffer = new ArrayBuffer(8);
            var tempDoubleBuffer = new Float64Array(tempBuffer);
            tempDoubleBuffer[0] = double;
            var tempByteBuffer = new Uint8Array(tempBuffer);
            var byteBuffer = new Uint8Array(newBuffer);
            var oldBuffer = new Uint8Array(this.Buffer);
            for (var i = 0; i < this.BufferSize; i++) {
                byteBuffer[i] = oldBuffer[i];
            }
            for (var j = this.BufferSize; j < this.BufferSize + 8; j++) {
                byteBuffer[j] = tempByteBuffer[j - this.BufferSize];
            }
            delete this.Buffer;
            this.Buffer = newBuffer;
            this.BufferPnt += 8;
            this.BufferSize += 8;
        },

        AddString: function (string)
        {
            var sArr = [];
            for(var i=0; i< string.length; i++)
            {
                var code = string.charCodeAt(i);
                if(code<0x80) sArr.push(code);
                else
                {
                    var uri = encodeURIComponent(string[i]);
                    uri=uri.replace("%","");
                    var uricode = uri.split("%");
                    for(var j=0; j<uricode.length; j++)
                        sArr.push(parseInt(uricode[j], 16));
                }
            }

            var len = sArr.length;
            encode.output = [];
            encode.outputSize = 0;
            encodeVarint(len, encode);
            /*encode.output[0] = len;
            encode.output[1] = 0;
            encode.outputSize = 2;*/

            var newBuffer = new ArrayBuffer(this.BufferSize + len + encode.outputSize);
            var byteBuffer = new Uint8Array(newBuffer);
            var oldBuffer = new Uint8Array(this.Buffer);
            for (var i = 0; i < this.BufferSize; i++)
            {
                byteBuffer[i] = oldBuffer[i];
            }
            for (var k = 0; k < encode.outputSize; k++)
            {
                byteBuffer[this.BufferSize + k] = encode.output[k];
            }
            for (var j = this.BufferSize + encode.outputSize; j < this.BufferSize + len + encode.outputSize; j++)
            {
                byteBuffer[j] = sArr[j - (this.BufferSize + encode.outputSize)];
            }
            delete this.Buffer;
            this.Buffer = newBuffer;
            this.BufferPnt += (len + encode.outputSize);
            this.BufferSize += (len + encode.outputSize);
        },

        AddData: function(data)
        {
            var len = data.BufferSize;
            encode.output = [];
            encode.outputSize = 0;
            encodeVarint(len, encode);
            var newBuffer = new ArrayBuffer(this.BufferSize + len + encode.outputSize);
            var byteBuffer = new Uint8Array(newBuffer);
            var oldBuffer = new Uint8Array(this.Buffer);
            for (var i = 0; i < this.BufferSize; i++) {
                byteBuffer[i] = oldBuffer[i];
            }
            for (var k = 0; k < encode.outputSize; k++) {
                byteBuffer[this.BufferSize + k] = encode.output[k];
            }
            var dataBuffer = new Uint8Array(data.Buffer);
            for (var j = this.BufferSize + encode.outputSize; j < this.BufferSize + len + encode.outputSize; j++) {
                byteBuffer[j] = dataBuffer[j - (this.BufferSize + encode.outputSize)];
            }
            delete this.Buffer;
            this.Buffer = newBuffer;
            this.BufferPnt += (len + encode.outputSize);
            this.BufferSize += (len + encode.outputSize);
        },

        GetChar: function ()
        {
            var byteBuffer = new Int8Array(this.Buffer);
            var char = byteBuffer[this.BufferPnt];
            this.BufferPnt++;
            return char;
        },

        GetByte: function () {
            var byteBuffer = new Uint8Array(this.Buffer);
            var byte = byteBuffer[this.BufferPnt];
            this.BufferPnt++;
            return byte;
        },

		GetInt8: function ()
		{
			var byteBuffer = new Int8Array(this.Buffer);
			var int8 = byteBuffer[this.BufferPnt];
			this.BufferPnt++;
			return int8;
		},

        GetBool: function () {
            var byteBuffer = new Uint8Array(this.Buffer);
            var bool = byteBuffer[this.BufferPnt];
            this.BufferPnt++;
            if(bool) return true;
            return false;
        },

        GetShort: function () {
            var byteBuffer = new Uint8Array(this.Buffer, this.BufferPnt);
            var short = byteBuffer[0]
                + byteBuffer[1] * 0x100;
            this.BufferPnt += 2;
            return short;
        },
		
		GetInt32: function ()
		{
			var byteBuffer = new Uint8Array(this.Buffer, this.BufferPnt);
            var int32 = byteBuffer[0]
                + byteBuffer[1] * 0x100
                + byteBuffer[2] * 0x10000
                + byteBuffer[3] * 0x1000000;
            this.BufferPnt += 4;
			return int32;
        },

        GetUint32: function () {
            var byteBuffer = new Uint8Array(this.Buffer, this.BufferPnt);
            var uint32 = byteBuffer[0]
                + byteBuffer[1] * 0x100
                + byteBuffer[2] * 0x10000
                + byteBuffer[3] * 0x1000000;
            this.BufferPnt += 4;
            return uint32;
        },

        GetUint64: function () {
            var byteBuffer = new Uint8Array(this.Buffer, this.BufferPnt);
            var uint64 = byteBuffer[0]
                + byteBuffer[1] * 0x100
                + byteBuffer[2] * 0x10000
                + byteBuffer[3] * 0x1000000
                + byteBuffer[4] * 0x100000000
                + byteBuffer[5] * 0x10000000000
                + byteBuffer[6] * 0x1000000000000
                + byteBuffer[7] * 0x100000000000000;
            this.BufferPnt += 8;
            return uint64;
        },

        GetDouble: function () {
            var byteBuffer = new Uint8Array(this.Buffer, this.BufferPnt);
            var newBuffer = new ArrayBuffer(8);
            var tempByteBuffer = new Uint8Array(newBuffer);
            for (var i = 0; i < 8; i++) {
                tempByteBuffer[i] = byteBuffer[i];
            }
            var doubleBuffer = new Float64Array(newBuffer);
            var double = doubleBuffer[0];
            this.BufferPnt += 8;
            return double;
        },

        GetString: function ()
        {
            var byteBuffer = new Uint8Array(this.Buffer, this.BufferPnt);
            decode.input = [];
            decode.size = 0;
            for (var k = 0; k < 10; k++)
            {
                decode.input[k] = byteBuffer[k];
            }
            var len = decodeVarint(decode);

            //var len = this.GetShort();
            var string = "";

            //for (var i = 2; i < len + 2; i++)
            for (var i = decode.size; i < len + decode.size; i++)
            {
                //string += String.fromCharCode(byteBuffer[i]);
                if(byteBuffer[i]<0x80)
                {
                    string += decodeURIComponent("%"+byteBuffer[i].toString(16));
                }
                else if(byteBuffer[i]>=0xC0 && byteBuffer[i]<=0xDF)
                {
                    string += decodeURIComponent("%"+byteBuffer[i].toString(16)+"%"+byteBuffer[i+1].toString(16));
                    i++;
                }
                else if(byteBuffer[i]>=0xE0 && byteBuffer[i]<=0xEF)
                {
                    string += decodeURIComponent("%"+byteBuffer[i].toString(16)+"%"+byteBuffer[i+1].toString(16)+"%"+byteBuffer[i+2].toString(16));
                    i+=2;
                }
            }
            this.BufferPnt += len + decode.size;
            return string;
        },

        GetData: function ()
        {
            var byteBuffer = new Uint8Array(this.Buffer, this.BufferPnt);
            decode.input = [];
            decode.size = 0;
            for (var k = 0; k < 10; k++) {
                decode.input[k] = byteBuffer[k];
            }
            var len = decodeVarint(decode);
            var data = new Serialize();
            for (var i = decode.size; i < len + decode.size; i++)
            {
                 data.AddByte(byteBuffer[i]);
            }
            this.BufferPnt += len + decode.size;
            data.Reset();
            return data;
        }
    };
    return Public;
};
